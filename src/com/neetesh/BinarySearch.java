package com.neetesh;

import java.util.ArrayList;
import java.util.Arrays;

public class BinarySearch {
    public static int binarySearch(ArrayList<Integer>arr,int begin , int end,int findNumber){
        if(begin>end){
            return -1;
        }
        int mid = (begin+end)/2;
        if(arr.get(mid)==findNumber){
            return (mid);
        }else if(arr.get(mid)>findNumber){
            return binarySearch(arr,begin,mid-1,findNumber);
        }else if(arr.get(mid)<findNumber){
            return binarySearch(arr,mid+1,end,findNumber);
        }
        return -1;
    }

    public static int binarySearchLowerBound(ArrayList<Integer>arr,int begin , int end,int findNumber){

        while (begin<end){
            int mid = begin+end;
            mid/=2;
            if(arr.get(mid)>=findNumber){
                end = mid;
            }else{
                begin = mid+1;
            }
        }
        return  begin;

    }

    public static int binarySearchUpperBound(ArrayList<Integer>arr,int begin , int end,int findNumber){
        while (begin<end){
            int mid = begin+end;
            mid/=2;
            if(arr.get(mid)<=findNumber){
                begin = mid+1;
            }else{
                end = mid;
            }
        }
        return  begin;
    }

    public static void main(String[] args) {

        Integer[] a = new Integer[]{1,3,5,7,9,13,15,16,16,16,18};
        ArrayList<Integer>arr = new ArrayList<>(Arrays.asList(a));
        System.out.println(binarySearchLowerBound(arr,0,arr.size(),0));


    }



}
