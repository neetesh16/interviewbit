package com.neetesh;

import java.util.ArrayList;

public class ReverseArray {
    static int i=0;
    public static void reverse(ArrayList<Integer>arr,int pos){
        if(pos==arr.size()){
            return;
        }
        int element = arr.get(pos);
        reverse(arr,pos+1);
        arr.set(i,element);
        i++;
    }

}
