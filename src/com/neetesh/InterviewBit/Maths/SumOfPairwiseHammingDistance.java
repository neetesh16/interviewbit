package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SumOfPairwiseHammingDistance {
    public static int hammingDistance(final List<Integer> A) {
        long MOD = 1000000007;
        long len = A.size();
        long result = 0L;
        long p = 1L;
        for(int i=0;i<32;i++){

            long c = 0;
            for(int j=0;j<len;j++){
                long temp = A.get(j);
                if( (temp & p) ==0){
//                    System.out.println(((1L<<i)) +" "+p);
                    c++;
                }
            }
            p = p*2L;
            result = result%MOD +  (2L*c*(len-c))%MOD;
            result = result%MOD;
        }
        int ans = (int)(result);
        return ans;

    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
        a = new Integer[]{96, 96, 7, 81, 2, 13};;
        System.out.println(hammingDistance(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{1, 2};;
        System.out.println(hammingDistance(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{1, 3, 5};;
        System.out.println(hammingDistance(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{-8, -7, -6};;
//        System.out.println(hammingDistance(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{1,2,3};
//        System.out.println(hammingDistance(new ArrayList<>(Arrays.asList(a))));

    }
}
