package com.neetesh.InterviewBit.Maths;

public class ReverseInteger {

    public static int reverse(int a){
        int orignal = a;
        int reverse = 0;
        boolean negNumber = false;
        if(a<0)negNumber = true;
        a = Math.abs(a);
        while (a>0){
            reverse = reverse*10+a%10;
            a=a/10;

        }
        if(reverse<0)return 0;
        if(negNumber)reverse*=-1;
        if(reverse>=Integer.MIN_VALUE && reverse<=Integer.MAX_VALUE)
            return reverse;
        else
            return 0;


    }
    public static void main(String[] args) {
        System.out.println(reverse(-2147483647));
    }
}
