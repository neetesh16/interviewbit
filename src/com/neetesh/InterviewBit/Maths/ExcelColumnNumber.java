package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;

public class ExcelColumnNumber {

    public static int titleToNumber(String a) {
        int res = 0;
        int l = a.length();
        int base = 26;
        for (int i=0;i<l;i++){
            res= res *base + (a.charAt(i)-'A'+1);
        }

        return res;
    }



    public static int titleToNumber2(String A) {
        int result = 0;
        for (int i = 0; i < A.length(); i++) {
            char ch = A.charAt(i);
            result = result * 26 + (ch - 'A' + 1);
        }
        return result;
    }
    public static void main(String[] args) {
//        primesum(100);
//        primesum(98);
        int k = titleToNumber("ABC");
        int l = titleToNumber2("CDA");
        System.out.println(k+" "+l);
//        primesum(6);
//        primesum(2);
    }
}
