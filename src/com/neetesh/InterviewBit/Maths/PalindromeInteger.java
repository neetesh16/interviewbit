package com.neetesh.InterviewBit.Maths;

public class PalindromeInteger {

    public static boolean isPalindrome(int a){
        System.out.println(a);
//        if(a<0)return false;
        int orignal = a;
        int reverse = 0;
        while (a>=0){
            reverse = reverse*10+a%10;
            a=a/10;
        }
        System.out.println(reverse);
        return orignal==reverse;
    }
    public static void main(String[] args) {
        System.out.println(isPalindrome(-2147447412));
    }
}
