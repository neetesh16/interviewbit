package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RearrangeArray {
    public static void arrange( ArrayList<Integer> a) {
        int length = a.size();
        for(int i=0;i<length;i++){
            int element = a.get(a.get(i)%length)%length;
            a.set(i,a.get(i)+length*element);
        }
        for(int i=0;i<length;i++){
            a.set(i,a.get(i)/length);
        }
    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
        a = new Integer[]{4, 0, 2, 1, 3};
        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));
        arrange(p);
        System.out.println(p);


    }
}
