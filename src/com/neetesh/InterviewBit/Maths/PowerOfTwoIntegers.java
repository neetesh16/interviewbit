package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;

public class PowerOfTwoIntegers {


    public static int isPower(int A) {
        if(A==1)return 1;
        int i = 2;
        int k = i*i;
        int val = 1;
        while(k>=1 && k<=A){
           if( A%i==0){
               int temp = A;
               int pow = 0;
               val = 1;
               while(temp%i==0 && temp!=0){
                   pow++;
                   temp = temp/i;
                   val*=i;
               }
           }
            if(val==A){
                return 1;
            }
           i++;
           k = i*i;
        }
        return 0;
    }

    public static void main(String[] args) {
        System.out.println( isPower(100));
        System.out.println( isPower(225));
        System.out.println( isPower(1));
        System.out.println( isPower(2));
        System.out.println( isPower(3));
        System.out.println( isPower(4));
    }
}
