package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;

public class TrailingZerosInFactorial {

    public static int fizzBuzz(int A) {
        int result = 0;
        int n = 5;

        while(A/n!=0){
            result+= A/n;
            n*=5;
        }
        return result;
    }
    public static void main(String[] args) {
        System.out.println(fizzBuzz(5));
        System.out.println(fizzBuzz(10));
        System.out.println(fizzBuzz(15));
        System.out.println(fizzBuzz(25));
        System.out.println(fizzBuzz(100));
    }
}
