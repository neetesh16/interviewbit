package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;

public class PrimeSum {

    public static ArrayList<Integer> findPrime(int limit){
        int n = limit;
        ArrayList<Integer> primes = new ArrayList<>();
        for (int i=0;i<n;i++){
            primes.add(1);
        }
        for (int i=2;i*i<n;i++){
            if(primes.get(i)==1){
                for (int k=2*i;k<n;k+=i){
                    primes.set(k,0);
                }
            }
        }
        ArrayList<Integer> pList = new ArrayList<>();
        for (int i=2;i<n;i++){
            if(primes.get(i)==1) pList.add(i);
        }
        return pList;
    }

    public static boolean primeCheck(int n){

        if(n == 2) return true;
        if(n % 2 == 0)
            return false;
        for(int i = 2; i < n; i++){
            if(n % i == 0)
                return false;
        }
        return true;
    }


    public static ArrayList<Integer> primesum(int a) {

        ArrayList<Integer> result = new ArrayList<Integer>();
        if(a < 2)
            return result;

        int left = 2;
        int right = a - left;
        while(left <= right){
            right = a - left;
            if(primeCheck(right) && primeCheck(left)){
                result.add(left);
                result.add(right);
                break;
            }
            left++;
        }
        return result;

    }
    //        ArrayList<Integer> res = new ArrayList<>();
//        ArrayList<Integer> primes = findPrime(a);
//        int l = primes.size()-1;
//        int r = 0;
//        while (r<=l){
//            if(a-primes.get(r) == primes.get(l)){
//                res.add(r);
//                res.add(l);
//                System.out.println(primes.get(r)+ " "+primes.get(l));
//                break;
//            }else if(a< primes.get(l)+primes.get(r)){
//                l--;
//            }
//            else if(a > primes.get(l)+primes.get(r)){
//                r++;
//            }
//        }
//        return res;
//    public static int binarySearch(ArrayList<Integer>arr,int begin , int end,int findNumber){
//        if(begin>end){
//            return -1;
//        }
//        int mid = (begin+end)/2;
//        if(arr.get(mid)==findNumber){
//            return (mid);
//        }else if(arr.get(mid)>findNumber){
//            return binarySearch(arr,begin,mid-1,findNumber);
//        }else if(arr.get(mid)<findNumber){
//            return binarySearch(arr,mid+1,end,findNumber);
//        }
//        return -1;
//    }

    //        for (int i=0;i<l;i++){
//            int p = binarySearch(primes,i,l-1,k-primes.get(i));
//            if(p!=-1){
//                res.add(i);
//                res.add(p);
//                System.out.println(primes.get(i)+ " "+primes.get(p));
//                break;
//            }
//        }

    public static void main(String[] args) {
//        primesum(100);
//        primesum(98);
        primesum(16777214);
//        primesum(6);
//        primesum(2);
    }
}
