package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;
import java.util.Arrays;

public class GridUniquePaths {
    public static int calculateCost(int[][] dp,int a,int b,int i,int j){
        if(a==i && b==j)return 1;
        if(i>a || j>b)return 0;
        if(dp[i][j]!=0) return dp[i][j];
        dp[i][j] = calculateCost(dp,a,b,i+1,j)+calculateCost(dp,a,b,i,j+1);
        return dp[i][j];
    }
    public static int uniquePaths( int A,int B) {
        int[][] dp = new int[A][B];
        A--;B--;
        int cost = calculateCost(dp,A,B,0,0);
        return cost;
    }

    public static void main(String[] args) {
        System.out.println(uniquePaths(1,3000));
    }
}
