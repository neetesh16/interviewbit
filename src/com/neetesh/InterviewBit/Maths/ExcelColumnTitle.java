package com.neetesh.InterviewBit.Maths;

public class ExcelColumnTitle {

    public static String convertToTitle(int A) {
        String res = "";
        int base =26;
        while (A>0){
            int remainder = A%base;
            A = A/base;
            if(remainder==0){A--;remainder=base;}
//            System.out.println(remainder+ " "+A);
            char c = (char)(remainder+'A'-1);
            res+=c+"";
        }
        String reverse = new StringBuffer(res).reverse().toString();
        return reverse;
    }

    public static int titleToNumber2(String A) {
        int result = 0;
        for (int i = 0; i < A.length(); i++) {
            char ch = A.charAt(i);
            result = result * 26 + (ch - 'A' + 1);
        }
        return result;
    }

    public static void main(String[] args) {
        String k = convertToTitle(titleToNumber2("SFDSF") );
        System.out.println(k);
    }
}
