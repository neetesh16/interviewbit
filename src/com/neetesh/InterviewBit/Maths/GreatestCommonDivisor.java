package com.neetesh.InterviewBit.Maths;

public class GreatestCommonDivisor {

    public static int gcd(int a,int b) {
        if(b==0)return a;
        return gcd(b,a%b);
    }

    public static void main(String[] args) {
        int k = gcd(0,3);
        System.out.println(k);
    }
}
