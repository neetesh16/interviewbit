package com.neetesh.InterviewBit.Maths;

import java.util.ArrayList;
import java.util.Arrays;

public class NumbersOfLengthNAndValueLessThanK {

    private static int solve(ArrayList<Integer> A, int B, int C) {
        int len = A.size();
        C--;
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(new Integer[]{0,0,0,0,0,0,0,0,0,0}));
        int eleCount = 0;
        for(int i=0;i<len;i++){
            numbers.set(A.get(i),1);
        }

        for(int i=0;i<10;i++){
            if(numbers.get(i)>0 ){
                eleCount++;
            }
        }
        int result = 1;
        ArrayList<Integer> toFind = new ArrayList<>();
        int k = C;
        while (k!=0){
            toFind.add(k%10);
            k/=10;
        }
        if(toFind.size()>B){
            toFind =  new ArrayList<>();
            for(int i=0;i<B;i++){
                toFind.add(9);
            }
        }else if(toFind.size()<B){
            return 0;
        }
        result = 0;
        for(int i=0;i<B;i++){
            int min = 0;
            int max = toFind.get(i);
            if(i==B-1 && B!=1){
                min = 1;
            }
            if(i==0){
                for(int j=0;j<10;j++){
                    if(numbers.get(j)>0 && j<=max && j>=min ){
                        result++;
                    }
                }
            }else{
                int currCount = 0;
                for(int j=0;j<10;j++){
                    if(numbers.get(j)>0 && j<max && j>=min ){
                        currCount++;
                    }
                }
                int excluding  = currCount*(int)Math.pow(eleCount,i);
                if(numbers.get(max)<=0)result = 0;
                result = result + excluding;
            }
        }

        return result;
    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{0,1,2,3,4,5,6,7,8,9};
        a = new Integer[]{0 };
        int k = solve(new ArrayList<>(Arrays.asList(a)),1,5);
        System.out.println(k);

        a = new Integer[]{0,1,2,5 };
        k = solve(new ArrayList<>(Arrays.asList(a)),2,21);
        System.out.println(k);
    }
}
