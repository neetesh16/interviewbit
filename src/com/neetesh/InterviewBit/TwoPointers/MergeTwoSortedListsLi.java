package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class MergeTwoSortedListsLi {
    static void  out(Object l){
        System.out.println(l);
    }

    public static void merge( ArrayList<Integer> A, ArrayList<Integer> B){

        int alen = A.size();
        int blen = B.size();
        int addLast = Math.abs(alen-blen);
        for (int i=alen;i<alen+blen;i++){
            A.add(-1);
        }
        int newLen = alen+blen;
        int k = newLen-1;
        for (int i=alen-1;i>=0;i--){
            A.set(k,A.get(i));
            k--;
        }
        k++;
        int j = 0;
        int i = 0;
        while (i<newLen){
            int temp = 0;
            if(k>=newLen){
                temp = B.get(j);
                j++;
            }else if(j>=blen){
                temp = A.get(k);
                k++;
            }else{
                if(A.get(k).compareTo(B.get(j))<0){
                    temp = A.get(k);
                    k++;
                }else{
                    temp = B.get(j);
                    j++;
                }
            }
            A.set(i,temp);
            i++;
        }
    }

    public static void main(String[] args) {
        ArrayList<Integer>A = new ArrayList<Integer>(Arrays.asList(new Integer[]{-4, 3  }));
        ArrayList<Integer>B = new ArrayList<Integer>(Arrays.asList(new Integer[]{-2, -2 }));

        merge(A,B);
        out(A);
//        A = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 1 }));
//        B = new ArrayList<Integer>(Arrays.asList(new Integer[]{ }));
//
//        out(B);
//        out(merge(A,B));

//
//        A = new ArrayList<Integer>(Arrays.asList(new Integer[]{10000000 }));
//        B = new ArrayList<Integer>(Arrays.asList(new Integer[]{10000000}));
//        out(intersect(A,B));
    }
}
