package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveDuplicatesfromSortedArray2 {

    private static int removeDuplicates(ArrayList<Integer> a) {
        int len = a.size();
        if(len<=2)return len;
        int pos = 1;
        int startPos = 0;
        for(int i=1;i<len;i++){
            if(!a.get(i-1).equals(a.get(i))){
                startPos = i;
                a.set(pos,a.get(i));
                pos++;
            }else{
                if(i> 1 && !a.get(i-2).equals(a.get(i))){
                    a.set(pos,a.get(i));
                    pos++;
                }else{
                    if(i<1 || startPos>i-2){
                        a.set(pos,a.get(i));
                        pos++;
                    }
                }

            }
        }
//        System.out.println(a.subList(0,pos));
        return pos;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
        a = new Integer[]{1,1,1,4,2,2,2};
        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
//        a = new Integer[]{1,1,1,1,1,1,1,1,2,2};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
        a = new Integer[]{1, 2,2,2, 2,2,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3, 3, 4, 4,4, 4,5,5,5, 5, 5};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));
//
//        a = new Integer[]{1, 1};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
//        a = new Integer[]{1};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
//        a = new Integer[]{};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
    }

}
