package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class SortByColor {

    private static int removeDuplicates(ArrayList<Integer> a) {
        int len = a.size();
        int pos = 1;
        int low = 0;
        int mid = 0;
        int high = len-1;
        while (mid<=high){
            if(a.get(mid).equals(0)){
                int t= a.get(low);
                a.set(low,a.get(mid));
                a.set(mid,t);
                mid++;
                low++;
            }else if(a.get(mid).equals(1)){
                 mid++;
                continue;
            } else if(a.get(mid).equals(2)){
                int t= a.get(high);
                a.set(high,a.get(mid));
                a.set(mid,t);
                high--;
            }
        }
        System.out.println(a);
        return pos;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};

        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
//        a = new Integer[]{1,1,1,1,1,1,1,1,2,2};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
        a = new Integer[]{0 ,1 ,2 ,0 ,1 ,2};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));
//
//        a = new Integer[]{1, 1};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
//        a = new Integer[]{1};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
//
//        a = new Integer[]{};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeDuplicates(p));
    }

}
