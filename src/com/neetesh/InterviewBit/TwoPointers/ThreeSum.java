package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ThreeSum {
    static void  out(Object l){
        System.out.println(l);
    }

    public static int  threeSumClosest(ArrayList<Integer>a,int sum){
        int res = Integer.MAX_VALUE-1;
        int ans = Integer.MAX_VALUE-1;
        int l = a.size();
        Collections.sort(a);
        for (int i=0;i<l-2;i++){
            int k = sum - a.get(i);
            int left = i+1;
            int right = l-1;
            int flag = 0;
            while (left<right){
                int p = Math.abs(a.get(left)+a.get(right)+a.get(i)-sum);
                if(res>=p){
                    res = p;
                    ans = a.get(left)+a.get(right)+a.get(i);
                }
                if(a.get(left)+a.get(right)>k){
                    right--;
                }else if(a.get(left)+a.get(right)<k){
                    left++;
                }else{
                    return ans;
                }

            }

        }
        return ans;
    }

    public static void main(String[] args) {
        ArrayList<Integer>a = new ArrayList<Integer>(Arrays.asList(new Integer[]{-1 ,2 ,1 ,-4  }));
        ArrayList<Integer>b = new ArrayList<Integer>(Arrays.asList(new Integer[]{-5, 1, 4, -7, 10, -7, 0, 7, 3, 0, -2, -5, -3, -6, 4, -7, -8, 0, 4, 9, 4, 1, -8, -6, -6, 0, -9, 5, 3, -9, -5, -9, 6, 3, 8, -10, 1, -2, 2, 1, -9, 2, -3, 9, 9, -10, 0, -9, -2, 7, 0, -4, -3, 1, 6, -3 }));
        ArrayList<Integer>c = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 2, 3, 6, 6  }));

        out(threeSumClosest(b,-1));
//        a = new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 4, 5, 8, 10  }));
//        b = new ArrayList<Integer>(Arrays.asList(new Integer[]{6, 9, 10 }));
//        c = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 2, 3, 6, 10  }));
//
//        out(solve(a,b,c));
    }
}
