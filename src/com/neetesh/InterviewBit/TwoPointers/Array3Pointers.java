package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Array3Pointers {
    public static int  Max(int A,int B,int C){
        return Math.max(Math.max(A,B),C);

    }

    public static int  Min(int A,int B,int C){
        return Math.min(Math.min(A,B),C);

    }

    public static int minimize(final List<Integer> A, final List<Integer> B, final List<Integer> C) {
        int res = Integer.MAX_VALUE-1;
        int i=0,j=0,k=0;
        int alen = A.size()-1;
        int blen = B.size()-1;
        int clen = C.size()-1;
        while ((i<=alen) && (j<=blen) && (k<=clen)){

            int p = A.get(i);
            int q = B.get(j);
            int r = C.get(k);

            int min = Min(p,q,r);
            res = Math.min(res,Max(Math.abs(p-q),Math.abs(r-q),Math.abs(p-r)));
            if(min==p)i++;
            else if(min==q)j++;
            else k++;
        }

        return res;
    }

    static void  out(Object l){
        System.out.println(l);
    }

    public static void main(String[] args) {
        ArrayList<Integer>A = new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 4, 10 }));
        ArrayList<Integer>B = new ArrayList<Integer>(Arrays.asList(new Integer[]{2, 15, 20}));
        ArrayList<Integer>C = new ArrayList<Integer>(Arrays.asList(new Integer[]{10, 12 }));

        out(minimize(A,B,C));

        A = new ArrayList<Integer>(Arrays.asList(new Integer[]{20, 24, 100 }));
        B = new ArrayList<Integer>(Arrays.asList(new Integer[]{2, 19, 22, 79, 800}));
        C = new ArrayList<Integer>(Arrays.asList(new Integer[]{10, 12, 23, 24, 119 }));

        out(minimize(A,B,C));
    }
}
