package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntersectionOfSortedArrays {
    static void  out(Object l){
        System.out.println(l);
    }

    public static ArrayList<Integer> intersect(final List<Integer> A, final List<Integer> B){

        int alen = A.size();
        int blen = B.size();
        ArrayList<Integer>ans = new ArrayList<>();
        int i=0;int j=0;
        while (i<alen && j<blen){
            int val = A.get(i).compareTo(B.get(j));
            if(val==0){
                ans.add(A.get(i));
                i++;j++;
            }else if(val>0){
                j++;
            }else{
                i++;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        ArrayList<Integer>A = new ArrayList<Integer>(Arrays.asList(new Integer[]{1 ,2, 3, 3, 4, 5, 6  }));
        ArrayList<Integer>B = new ArrayList<Integer>(Arrays.asList(new Integer[]{3 ,3 ,5 }));

        out(intersect(A,B));
        A = new ArrayList<Integer>(Arrays.asList(new Integer[]{1 ,2, 3, 3, 4, 5, 6  }));
        B = new ArrayList<Integer>(Arrays.asList(new Integer[]{3,5 }));
        out(intersect(A,B));

        A = new ArrayList<Integer>(Arrays.asList(new Integer[]{10000000 }));
        B = new ArrayList<Integer>(Arrays.asList(new Integer[]{10000000}));
        out(intersect(A,B));
    }
}
