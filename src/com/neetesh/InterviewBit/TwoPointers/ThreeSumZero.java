package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ThreeSumZero {
    static void  out(Object l){
        System.out.println(l);
    }

    public static ArrayList<ArrayList<Integer>>  threeSum(ArrayList<Integer>a){

        int l = a.size();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        Collections.sort(a);
        int pos = -1;
        for (int i=0;i<l-2;i++){
            if(i>0 && a.get(i)==a.get(i-1))continue;
            int k = 0 - a.get(i);
            int left = i+1;
            int right = l-1;
            int flag = 0;
            while (left<right){
                int p = Math.abs(a.get(left)+a.get(right)+a.get(i));

                if(p==0){
                    ArrayList<Integer> temp = new ArrayList<>();
                    temp.add(a.get(i));
                    temp.add(a.get(left));
                    temp.add(a.get(right));
                    if (pos>=0){
                        ArrayList<Integer> temp2 = res.get(pos);
                        if(temp2.get(0).equals(temp.get(0)) && temp2.get(1).equals(temp.get(1)) && temp2.get(2).equals(temp.get(2))){

                        }else{
                            res.add(temp);
                            pos++;
                        }
                    }else{
                        res.add(temp);
                        pos++;
                    }


                }
                if(a.get(left)+a.get(right)>k){
                    right--;
                }else if(a.get(left)+a.get(right)<=k){
                    left++;
                }

            }

        }
        return res;
    }

    public static void main(String[] args) {
        ArrayList<Integer>a = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 1, -4, 0, 0, 5, -5, 1, 0, -2, 4, -4, 1, -1, -4, 3, 4, -1, -1, -3  }));
        ArrayList<Integer>b = new ArrayList<Integer>(Arrays.asList(new Integer[]{-31013930, -31013930, 9784175, 21229755 }));
//        ArrayList<Integer>c = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 2, 3, 6, 6  }));

        out(threeSum(b));
//        a = new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 4, 5, 8, 10  }));
//        b = new ArrayList<Integer>(Arrays.asList(new Integer[]{6, 9, 10 }));
//        c = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 2, 3, 6, 10  }));
//
//        out(solve(a,b,c));
    }
}
