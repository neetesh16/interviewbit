package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class DiffK {

    public static int diffPossible(ArrayList<Integer> A,int B){

        int low = 0;
        int high = A.size() - 1;
        int l = high+1;
        high = 1;
        while (low<l && high < l){

            if (A.get(high)-A.get(low)== B){
                return 1;
            }else if (A.get(high)-A.get(low) < B){
                high++;
            }else{
                low++;
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList( 1, 2, 2, 3, 4));
        System.out.println(diffPossible(A,0));
        A = new ArrayList<Integer>(Arrays.asList(1, 3, 5));
        System.out.println(diffPossible(A,4));

        A = new ArrayList<Integer>(Arrays.asList(1, 3, 5));
        System.out.println(diffPossible(A,6));
//        A = new ArrayList<Integer>(Arrays.asList(5, 1, 3, 2, 4));
//        System.out.println(wave(A));
    }
}
