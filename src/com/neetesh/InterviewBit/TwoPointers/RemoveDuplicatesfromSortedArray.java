package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveDuplicatesfromSortedArray {

    private static int removeDuplicates(ArrayList<Integer> A) {
        int len = A.size();
        if(len==0)return 0;
        int pos = 1;
        for(int i=1;i<len;i++){
            if(!A.get(i).equals(A.get(i-1))){
                 A.set(pos,A.get(i));
                 pos++;
            }
        }
//        System.out.println(A);
        return pos;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
        a = new Integer[]{1,1,2,3,4};
        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));

        a = new Integer[]{1,1,2};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));

        a = new Integer[]{1, 2, 2, 3, 4, 4, 4, 5, 5};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));

        a = new Integer[]{1, 1};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));

        a = new Integer[]{1};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));

        a = new Integer[]{};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeDuplicates(p));
    }

}
