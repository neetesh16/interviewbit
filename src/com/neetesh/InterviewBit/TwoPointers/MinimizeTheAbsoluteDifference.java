package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class MinimizeTheAbsoluteDifference {
    static void  out(Object l){
        System.out.println(l);
    }


    static int  Max(int a,int b,int c){
        return Math.max(Math.max(a,b),c);

    }

    static int  Min(int a,int b,int c){
        return Math.min(Math.min(a,b),c);

    }

    static int  solve(ArrayList<Integer>a,ArrayList<Integer>b,ArrayList<Integer>c){
        int res = Integer.MAX_VALUE-1;
        int i=0,j=0,k=0;
        int alen = a.size()-1;
        int blen = b.size()-1;
        int clen = c.size()-1;
        while ((i<=alen) && (j<=blen) && (k<=clen)){

            int p = a.get(i);
            int q = b.get(j);
            int r = c.get(k);

            int min = Min(p,q,r);
            res = Math.min(res,Max(p,q,r)-min);
            if(min==p)i++;
            else if(min==q)j++;
            else k++;
        }

        return res;
    }

    public static void main(String[] args) {
        ArrayList<Integer>a = new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 4, 5, 8, 10  }));
        ArrayList<Integer>b = new ArrayList<Integer>(Arrays.asList(new Integer[]{6, 9, 15 }));
        ArrayList<Integer>c = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 2, 3, 6, 6  }));

        out(solve(a,b,c));

        a = new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 4, 5, 8, 10  }));
        b = new ArrayList<Integer>(Arrays.asList(new Integer[]{6, 9, 10 }));
        c = new ArrayList<Integer>(Arrays.asList(new Integer[]{ 2, 3, 6, 10  }));

        out(solve(a,b,c));
    }
}
