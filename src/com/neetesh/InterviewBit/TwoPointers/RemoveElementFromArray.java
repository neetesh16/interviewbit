package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;

public class RemoveElementFromArray {

    private static int removeElement(ArrayList<Integer> a,int b) {
        int len = a.size();
        int pos = 0;
        for(int i=0;i<len;i++){
            if(a.get(i).equals(b)){
                continue;
            }else{
                a.set(pos,a.get(i));
                pos++;
            }
        }
//        System.out.println(a.subList(0,startPos));
        return pos;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
        a = new Integer[]{1,1,1,2,2,2,4,4,4,1,2,2,2,2,2,2,6,7,8,9};
        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));
        out(removeElement(p,2));

        a = new Integer[]{1,1,1,1,1,1,1,1,2,2};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeElement(p,1));

        a = new Integer[]{1, 1};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeElement(p,1));

        a = new Integer[]{4, 1, 1, 2, 1, 3};
        p = new ArrayList<>(Arrays.asList(a));
        out(removeElement(p,1));
//
//        a = new Integer[]{1, 1};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeElement(p));
//
//        a = new Integer[]{1};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeElement(p));
//
//        a = new Integer[]{};
//        p = new ArrayList<>(Arrays.asList(a));
//        out(removeElement(p));
    }

}
