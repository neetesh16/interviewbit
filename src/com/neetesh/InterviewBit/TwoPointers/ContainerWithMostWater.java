package com.neetesh.InterviewBit.TwoPointers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ContainerWithMostWater {

    public static int maxArea(ArrayList<Integer> A){

        int low = 0;
        int res = 0;
        int high = A.size() - 1;
        while (low<high){
            int area = Math.min(A.get(low),A.get(high));
            area = area*(high-low);
            res = Math.max(res,area);
            if (A.get(low)>A.get(high)){
                high--;
            }else{
                low++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<Integer>(Arrays.asList(1, 5, 4, 3));
        System.out.println(maxArea(A));
//        A = new ArrayList<Integer>(Arrays.asList(10,20));
//        System.out.println(wave(A));
//        A = new ArrayList<Integer>(Arrays.asList(5, 1, 3, 2, 4));
//        System.out.println(wave(A));
    }
}
