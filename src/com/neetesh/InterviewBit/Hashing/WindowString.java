package com.neetesh.InterviewBit.Hashing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WindowString {

    public static String minWindow(String S, String T) {
        int slen = S.length();
        int tlen = T.length();
        String ans = "";
        HashMap<Character, Integer> cmap = new HashMap<>();
        for (int i = 0; i < tlen; i++) {
            if (cmap.containsKey(T.charAt(i))) {
                cmap.put(T.charAt(i), cmap.get(T.charAt(i)) + 1);
            } else {
                cmap.put(T.charAt(i), 1);
            }
        }
        int startPtr = -1;
        int count = 0;
        int sptr = -1;
        int eptr = -1;
        int res = Integer.MAX_VALUE;
        int j = 0;
        for (int i = 0; i < slen; i++) {
            Character c = S.charAt(i);
            if (cmap.containsKey(c)) {
                if (startPtr == -1) {
                    startPtr = i;
                }
                int t = cmap.get(c)-1;
                if(t>=0)j++;
                cmap.put(c, t);

//                for (int u = 0; u < tlen; u++) {
//                    if (cmap.get(T.charAt(u)) <= 0)
//                        j++;
//                }
//                if (cmap.get(T.charAt(u)) <= 0)
//                        j++;
//                if (cmap.containsKey(S.charAt(i))) {
//                    if(cmap.get(S.charAt(i))<=0)j--;
//
//                }

                do {

                    if (j >= tlen) {
                        if (res > i + 1 - startPtr) {
                            res = i + 1 - startPtr;
                            sptr = startPtr;
                            eptr = i + 1;
                        }
                        if (cmap.containsKey(S.charAt(startPtr))) {
                            int r = cmap.get(S.charAt(startPtr));
                            if(r>=0)j--;
                            cmap.put(S.charAt(startPtr), cmap.get(S.charAt(startPtr)) + 1);
                        }
                        startPtr++;
                    }

                } while (j >= tlen && startPtr < i);
            }
        }
        if (sptr != -1) {
            ans = S.substring(sptr, eptr);
        }
        return ans;
    }


    static void out(Object l) {
        System.out.println(l);
    }

    public static void main(String[] args) {

        out(minWindow("AAAAAA", "AA"));
        out(minWindow("ADOBECODEBANC", "ABC"));
    }

}
