package com.neetesh.InterviewBit.Hashing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class PointsOnTheStraightLine {
    public static int equal(ArrayList<Integer> X,ArrayList<Integer> Y) {
        int len = X.size();
        HashMap<Double,Integer> cmap = new HashMap<>();
        int res = 0;
        for(int i= 0 ;i<len;i++){
            for(int j= 0 ;j<len;j++){
                if(i==j)continue;
                int num = Y.get(i)-Y.get(j);
                int den = X.get(i)-X.get(j);
                double slope = (num+0.0)/(den+0.0);
                if (cmap.containsKey(slope)){
                    int t = cmap.get(slope)+1;
                    res = Math.max(res,t);
                    cmap.remove(slope);
                    cmap.put(slope,t);

                }else{
                    res = Math.max(res,2);
                    cmap.put(slope,2);
                }
            }
        }
        return res;
    }


    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{-1,0,1,2,3,4};
        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));

        Integer[] b = new Integer[]{1,0,1,2,3,4};
        ArrayList<Integer> q = new ArrayList<>(Arrays.asList(b));
        out(equal(p,q));
    }
//4
}
