package com.neetesh.InterviewBit.Hashing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LargestContinuousSequenceZeroSum {
    public static ArrayList<Integer> lszero(ArrayList<Integer> A) {
        int l = A.size();
        HashMap<Integer,Integer> cmap = new HashMap<>();
        int res = -1;
        int sum = 0;
        ArrayList<Integer> ans =  new ArrayList<>();
        cmap.put(0,-1);
        for (int i=0;i<l;i++){
            sum+=A.get(i);
            if (cmap.containsKey(sum)){
                if(res < i-cmap.get(sum)){
                    res = Math.max(res,i-cmap.get(sum));
                    ans = new ArrayList<>(A.subList(cmap.get(sum)+1,i+1));
                }
            }else{
                cmap.put(sum,i);
            }
        }
        return  ans;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{1 ,2 ,-2 ,4 ,-4};
//        out(lszero(new ArrayList<Integer>(Arrays.asList(a))));
//
//        a = new Integer[]{15, -2, 2, -8, 1, 7, 10, 23};
//        out(lszero(new ArrayList<Integer>(Arrays.asList(a))));
//
//        a = new Integer[]{1,2,3};
//        out(lszero(new ArrayList<Integer>(Arrays.asList(a))));

        a = new Integer[]{1,2,3,4,-4,-3,-2,-1};
        out(lszero(new ArrayList<Integer>(Arrays.asList(a))));
    }

}
