package com.neetesh.InterviewBit.Hashing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class SubstringConcatenation {
    public static ArrayList<Integer> findSubstring(String S,final List<String> T) {
        int slen = S.length();
        int tlen = T.size();
        ArrayList<Integer> res = new ArrayList<>();
        HashMap<String,Integer>cmap = new HashMap<>();
        HashMap<String,Integer>copy = new HashMap<>();
        for (String s: T){
            if(copy.containsKey(s)){
                copy.put(s,copy.get(s)+1);

            }else{
                copy.put(s,1);
            }

        }
        int wLen = 0;
        if(tlen > 0 ){
            wLen = T.get(0).length();
        }else{
            return res;
        }
        int c = wLen;
        for (int j=0;j<slen;j++){
            cmap = new HashMap<>(copy);
            StringBuilder p = new StringBuilder(S);
            int y = j;

            int l = tlen;
            for (int i=j+wLen;i<=slen;i=i+wLen){
                String temp = p.subSequence(y,i).toString();
                if(cmap.containsKey(temp)){
                    if(cmap.get(temp) > 0){
                        l--;
                        cmap.put(temp,cmap.get(temp)-1);
                    }else{
                        break;
                    }

                }else{
                    if(l>0){
                        break;
                    }
                }

                y = i;
            }
            if (l<=0){
                res.add(j);
            }
        }


        return res;
    }


    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        String [] s = new String[]{"aaa", "aaa", "aaa", "aaa", "aaa"};
        ArrayList<String> a = new ArrayList<>(Arrays.asList(s));
        out(findSubstring("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",a));
    }

}
