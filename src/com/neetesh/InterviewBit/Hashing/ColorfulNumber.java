package com.neetesh.InterviewBit.Hashing;

import java.util.HashMap;

public class ColorfulNumber {
    public static int colorful(int A) {
        StringBuilder b = new StringBuilder(A+"");
        int l = b.length();
        HashMap<Integer,Integer> cmap = new HashMap<>();
        for(int i=0;i<l;i++){
            int product = 1;
            for (int j=i;j<l;j++){
                product = product*(b.charAt(j)-'0');
                if(cmap.containsKey(product)){
                    return 0;
                }else{
                    cmap.put(product,1);
                }
            }
        }
        return 1;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        out(colorful(22));
    }

}
