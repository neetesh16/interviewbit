package com.neetesh.InterviewBit.Hashing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Equal {
    public static ArrayList<Integer> equal(ArrayList<Integer> A) {
        int l = A.size();
        ArrayList<Integer> res = new ArrayList<>();
        HashMap<Integer,ArrayList<Integer> > cmap = new HashMap<>();
        for(int i=0;i<l;i++){
            for (int j=i+1;j<l;j++){
                int product = A.get(i)+A.get(j);
                if (cmap.containsKey(product)){
                    ArrayList<Integer> temp = new ArrayList<>(cmap.get(product));
                    temp.add(i);
                    temp.add(j);
                    if(temp.get(0)!=i && temp.get(1)!=j && temp.get(1)!=i){
                        res = compare(temp,res);
                    }

                }else{
                    ArrayList<Integer> temp = new ArrayList<>();
                    temp.add(i);
                    temp.add(j);
                    cmap.put(product,temp);
                }
            }
        }
        return res;
    }

    public static  ArrayList<Integer> compare( ArrayList<Integer> A, ArrayList<Integer>B){
        if(B.size()==0)return A;
        if(A.get(0)==A.get(2) || A.get(1)==A.get(3))return B;

        for (int i=0;i<4;i++){
            if(A.get(i)==B.get(i))continue;
            if(A.get(i)<B.get(i))return A;
            if(A.get(i)>B.get(i))return B;
        }
        return A;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{1, 1, 1, 1, 1};
        out(equal(new ArrayList<Integer>(Arrays.asList(a))));
    }

}
