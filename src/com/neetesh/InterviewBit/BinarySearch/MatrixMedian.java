package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MatrixMedian {

    public static int upperBound(List<Integer> a, int begin , int end, int findNumber){
        while (begin<end){
            int mid = (begin+end)/2;
            if(a.get(mid)<=findNumber){
                begin = mid+1;
            }else{
                end = mid;
            }
        }
        return begin;
    }
    public static int findMedian(ArrayList<ArrayList<Integer>> a) {
        if(a.size()==0)return 0;

        int min=Integer.MAX_VALUE,max = Integer.MIN_VALUE ;
        int l = a.get(0).size();
        int len = a.size();
        for(int i=0;i<len;i++){
            min = Math.min(min,a.get(i).get(0));
            max = Math.max(max,a.get(i).get(l-1));
        }
        int total = l*len+1;
        total/=2;
        while (min<max){
            int mid = max+min;
            mid/=2;
            int c = 0;
//            System.out.println("\nMID "+mid);
            for (int i=0;i<len;i++){
                int k = upperBound(a.get(i),0,l,mid);
                c+=k;
//                System.out.println("I "+i+" SMALLER "+k);
            }
            if(c>=total){
                max = mid;
            }else{
                min = mid+1;
            }
        }
        return  min;
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> a = new ArrayList<>();
        Integer[] a1 ;
        a1 = new Integer[]{1,3,5};
        a.add(new ArrayList<>(Arrays.asList(a1)));
        a1 = new Integer[]{2, 6, 9};
        a.add(new ArrayList<>(Arrays.asList(a1)));
        a1 = new Integer[]{3, 6, 9};
        a.add(new ArrayList<>(Arrays.asList(a1)));

        System.out.println(findMedian(a));

//        a.clear();
//        a1 = new Integer[]{1,3,5,7,10};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        a1 = new Integer[]{2, 6, 9,10,11};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        a1 = new Integer[]{3, 6, 9,11,12};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        a1 = new Integer[]{6, 9, 11,14,15};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        a1 = new Integer[]{11, 19, 111,114,115};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        findMedian(a);

//        ArrayList<Integer>p = new ArrayList<>();
//        for(int i=0;i<a.size();i++){
//           p.addAll(a.get(i));
//        }
//        Collections.sort(p);
//        System.out.println(p);
//        a.clear();
//        a1 = new Integer[]{1,3,5,7,10};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        setZeroes(a);
//        System.out.println(a);
//        for (int i=0;i<a.size();i++){
//            for (int j=0;j<a.get(i).size();j++){
//                System.out.print(a.get(i).get(j)+" ");
//            }
//            System.out.println("");
//        }
//        System.out.println();
    }
}
