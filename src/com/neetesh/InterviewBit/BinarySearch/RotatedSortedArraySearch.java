package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RotatedSortedArraySearch {

    public static int binarySearch(List<Integer>arr,int begin , int end,int findNumber){
        if(begin>end){
            return -1;
        }
        int mid = (begin+end)/2;
        if(arr.get(mid)==findNumber){
            return (mid);
        }else if(arr.get(mid)>findNumber){
            return binarySearch(arr,begin,mid-1,findNumber);
        }else if(arr.get(mid)<findNumber){
            return binarySearch(arr,mid+1,end,findNumber);
        }
        return -1;
    }

    public static int search(final List<Integer> a, int b) {
        int l = a.size();
        int low = 0;
        int high = l-1;
        while (low<high){
            int mid = high+low;
            mid/=2;
            if(mid >low &&  a.get(mid)<a.get(mid-1)){
                low = mid-1 ;
                break;
            }else if(mid < high && a.get(mid)>a.get(mid+1)){
                low = mid ;
                break;
            }else if(a.get(mid)<a.get(low)){
                high = mid-1;
            }else{
                low = mid+1;
            }
        }

        int ans = Math.max(binarySearch(a,0,low,b),binarySearch(a,low+1,l-1,b));
//        System.out.println("ans found "+ans);
        return ans;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        Integer[] a1 ;
//        a1 = new Integer[]{ 7,6,5,4,3,2,1};
        a1 = new Integer[]{7, 0 ,1, 2,4 ,5, 6 };
//        a1 = new Integer[]{ 4, 5, 6, 7, 8,9,10,0, 1, 2};
        a = (new ArrayList<>(Arrays.asList(a1)));



        ArrayList<Integer>p = new ArrayList<>();

        System.out.println(search(a,9));
    }
}
