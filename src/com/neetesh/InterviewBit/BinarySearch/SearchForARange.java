package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SearchForARange {

    public static int upperBound(List<Integer>a,int begin , int end,int findNumber){
        while (begin<end){
            int mid = (begin+end)/2;
            if(a.get(mid)<=findNumber){
                begin = mid+1;
            }else{
                end = mid;
            }
        }
        return begin;
    }

    public static int lowerBound(List<Integer>a,int begin , int end,int findNumber){
        while (begin<end){
            int mid = (begin+end)/2;
            if(a.get(mid)>=findNumber){
                end = mid;
            }else{
                begin = mid + 1;
            }
        }
        return begin;
    }
    public static ArrayList<Integer> searchRange(final List<Integer> a, int b) {
        ArrayList<Integer>res = new ArrayList<>();
        int l = a.size();
        int low = lowerBound(a,0,l,b);
        if(low <l && a.get(low)==b){
            res.add(low);
            int high = upperBound(a,0,l,b);
            res.add(high-1);
        }else{
            res.add(-1);
            res.add(-1);
        }

        System.out.println(res);
        return res;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        Integer[] a1 ;
//        a1 = new Integer[]{ 7,6,5,4,3,2,1};
        a1 = new Integer[]{5, 7, 7, 8, 9, 10 };
//        a1 = new Integer[]{ 4, 5, 6, 7, 8,9,10,0, 1, 2};
        a = (new ArrayList<>(Arrays.asList(a1)));

        System.out.println(searchRange(a,1));
    }
}
