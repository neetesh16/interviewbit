package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AllocateBooks {

    static void  out(Object l){
        System.out.println(l);
    }

    public static int lowerBound(ArrayList<Integer>A,int low,int high,int num){
        while (low<high){
            int mid = low+high;
            mid/=2;
            if(A.get(mid)>=num){
                high = mid;
            }else{
                low = mid+1;
            }
        }
        if(low==A.size()){
            --low;
            return low;
        }
        else if(low<= A.size()-1 && A.get(low)!=num)low--;
//        if(low<0)low=0;
        return low;
    }
    
    public static int books(ArrayList<Integer>A,int B){
        int l = A.size();
        if(B>l)return -1;
        int low = 0;
        low = Math.max(low,A.get(0));
        for (int i=1;i<l;i++){
            low = Math.max(low,A.get(i));
            A.set(i,A.get(i)+A.get(i-1));

        }
//        out(A);


        int high = A.get(l-1);
        int res = Integer.MAX_VALUE-1;
        int h = 0;
        while (low<=high){
            int mid = low+high;
            mid/=2;
            int x = mid;
            int y = 0;
            int j = 1;
            int resl = -1;

            for(j=1;j<B;j++){

                int p = lowerBound(A,y,l,x);
//                out(low+" "+h igh);
//                out("MID: "+mid +" Y: "+y+" X: "+x+" J "+j+ " P: "+p);
                int z = A.get(p);
                if(y!=0){
                    z-=A.get(y-1);

                }
                x = A.get(p)+mid;
                resl = Math.max(resl,z);
                y = p+1;
                if(y>=l)break;
            }
            if(j<=B){
                int o = 0;
                if(y!=0)o = A.get(y-1);
                resl = Math.max(resl,A.get(l-1)-o);
                res = Math.min(resl,res);
                if(resl > mid){
                    low = mid + 1;
                }else{
                    high = mid - 1;
                }
//                if(res>resl){
//                    res = resl;
////                    low = mid+1;
//
//                }else{
////                    low = mid+1;
//                }

            }else{
                high = mid - 1 ;
            }

        }
        return res;
    }

    public static void main(String[] args) {
//        Integer[] val = new Integer[]{12, 34, 67, 90};
        Integer[] val = new Integer[]{ 73, 58, 30, 72, 44, 78, 23, 9 };
        ArrayList A = new ArrayList(Arrays.asList(val));
//        out(books(A,5));
//
//        val = new Integer[]{ 12, 34, 67, 90 };
//        A = new ArrayList(Arrays.asList(val));
//        out(books(A,2));
//
//        val = new Integer[]{ 12, 34, 67, 90 };
//        A = new ArrayList(Arrays.asList(val));
//        out(books(A,3));
//
//        val = new Integer[]{97, 26, 12, 67, 10, 33, 79, 49, 79, 21, 67, 72, 93, 36, 85, 45, 28, 91, 94, 57, 1, 53, 8, 44, 68, 90, 24 };
//        A = new ArrayList(Arrays.asList(val));
//        out(books(A,26));
//
//        val = new Integer[]{ 24 };
//        A = new ArrayList(Arrays.asList(val));
//        out(books(A,1));

        val = new Integer[]{  640, 435, 647, 352, 8, 90, 960, 329, 859};
        A = new ArrayList(Arrays.asList(val));
        out(books(A,3));
    }
}
