package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SortedInsertPosition {

    public static int lowerBound(List<Integer>a,int begin , int end,int findNumber){
        while (begin<end){
            int mid = (begin+end)/2;
            if(a.get(mid)>=findNumber){
                end = mid;
            }else{
                begin = mid + 1;
            }
        }
        return begin;
    }
    public static int searchInsert(final List<Integer> a, int b) {
        int l = a.size();
        int res = lowerBound(a,0,l,b);
        return res;
    }
    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<>();
        Integer[] a1 ;
//        a1 = new Integer[]{ 7,6,5,4,3,2,1};
        a1 = new Integer[]{1,3,5,6 };
//        a1 = new Integer[]{ 4, 5, 6, 7, 8,9,10,0, 1, 2};
        a = (new ArrayList<>(Arrays.asList(a1)));

        System.out.println(searchInsert(a,7));
    }
}
