package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;

public class SquareRootofInteger {

    public static int sqrt(int a) {
        if(a<=1)return a;
        int low = 1;
        int high = a;
        int ans  = 1;
        while (low<=high){
            int mid = (high-low)/2;
            mid+=low;

            int p = a/mid;
            if(p==mid){
                ans = mid;
                break;
            }
            if(p<mid){
                high = mid-1;
                ans = Math.max(ans,p);
            }else{
                low = mid+1;

            }

        }
        return ans;
    }

    public static void main(String[] args) {

        System.out.println(sqrt(2147483647));
        System.out.println(sqrt(2147395600));
        System.out.println(sqrt(11));
        System.out.println(sqrt(2));
        System.out.println(sqrt(5));
        System.out.println(sqrt(225));
        System.out.println(sqrt(16));
        System.out.println(sqrt(10240000));
        System.out.println(sqrt(16));


    }
}
