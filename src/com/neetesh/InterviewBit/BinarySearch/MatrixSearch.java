package com.neetesh.InterviewBit.BinarySearch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MatrixSearch {

    public static int searchMatrix(ArrayList<ArrayList<Integer>> a, int b) {

        if(a.size()==0)return 0;
        int l = a.get(0).size();
        int len = a.size();
        int low = 0 ;
        int high = len;
        while (low<high){ //upper bound
            int mid = high+low;
            mid/=2;
            if(a.get(mid).get(0)<=b){
                low = mid+1;
            }else{
                high = mid;
            }
        }
        int ans = low;
        ans--;
        if(ans <= 0)ans=0;
        low = 0;
        high = l;
        while (low<high){//lower bound
            int mid = high+low;
            mid/=2;
            if(a.get(ans).get(mid)>=b){
                high = mid;
            }else{
                low = mid+1;
            }
        }
        if(low>=l)low=l-1;
        if(a.get(ans).get(low)==b){
            return 1;
        }else{
            return 0;
        }
    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> a = new ArrayList<>();
        Integer[] a1 ;
//        a1 = new Integer[]{1,3,5};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        a1 = new Integer[]{2, 6, 9};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        a1 = new Integer[]{3, 6, 9};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        setZeroes(a);
//        System.out.println(a);

        a.clear();
        a1 = new Integer[]{1,   3,  5,  7};
        a.add(new ArrayList<>(Arrays.asList(a1)));
        a1 = new Integer[]{10, 11, 16, 20};
        a.add(new ArrayList<>(Arrays.asList(a1)));
        a1 = new Integer[]{23, 30, 34, 50};
        a.add(new ArrayList<>(Arrays.asList(a1)));
        System.out.println(searchMatrix(a,20));

//        ArrayList<Integer>p = new ArrayList<>();
//        for(int i=0;i<a.size();i++){
//            p.addAll(a.get(i));
//        }
//        Collections.sort(p);
//        System.out.println(p);
//        a.clear();
//        a1 = new Integer[]{1,3,5,7,10};
//        a.add(new ArrayList<>(Arrays.asList(a1)));
//        setZeroes(a);
//        System.out.println(a);
//        for (int i=0;i<a.size();i++){
//            for (int j=0;j<a.get(i).size();j++){
//                System.out.print(a.get(i).get(j)+" ");
//            }
//            System.out.println("");
//        }
//        System.out.println();
    }
}
