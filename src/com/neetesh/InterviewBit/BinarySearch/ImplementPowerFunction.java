package com.neetesh.InterviewBit.BinarySearch;

public class ImplementPowerFunction {

    public static int pow(int x,int n,int d) {
        long ans = 1,y = x;
        while (n > 0){
            if(n%2!=0){
                ans=(ans*y)%d;
            }
            y=(y*y)%d;
            n/=2;
        }
        return (int)(ans%d);
    }

    public static void main(String[] args) {


        System.out.println(pow(Integer.MAX_VALUE,2,89));


    }
}
