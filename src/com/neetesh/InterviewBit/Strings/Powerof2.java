package com.neetesh.InterviewBit.Strings;

public class Powerof2 {


    public static String addString(String A,String B){
        int alen = A.length();
        int blen = B.length();
        int i = 0;
        int j = 0;

        StringBuffer c = new StringBuffer("");
        int carry = 0;
        while (i<alen || j<blen){
            int tempAdd  = (i<alen?A.charAt(i):48)+(j<blen?B.charAt(j):48)-2*48+carry;
            carry = tempAdd/10;
            tempAdd = tempAdd%10;
            c.append(tempAdd+"");
            i++;j++;
        }
        while (carry!=0){
            c.append(carry%10+"");
            carry/=10;
        }
        return c.toString();
    }


    public static String multiply(String A, String B) {

        int alen = A.length();
        int blen = B.length();
        int i = alen-1;
        String ans = "";

        int k = 0;
        int mulcarry = 0;
        int addcarry = 0;
        StringBuffer x = new StringBuffer("");
        while (i>=0){
            int up = A.charAt(i)-48;
            int j = blen-1;
            mulcarry = 0;
            addcarry = 0;
            k = 0;

            StringBuffer c = new StringBuffer(x);
            while (j>=0){
                int down = B.charAt(j)-48;
                int mul = up*down+mulcarry;
                int num = mul%10;
                mulcarry = mul/10;
                c.append(num);

                j--;
            }
            while (mulcarry!=0){
                c.append(mulcarry%10+"");
                mulcarry/=10;
            }

            ans = addString(ans,c.toString());
            x.append("0");
            i--;
        }
        StringBuffer res = new StringBuffer(ans).reverse();


        return res.toString();
    }
    public static String divide(String A){
        int i=0;
        int l = A.length();
        StringBuilder res = new StringBuilder("");
        int carry = 0;
        int temp = 0;
        while (i<l){
            temp = carry*10+(A.charAt(i)-48);
            carry = temp%2;
            temp/=2;
            if(temp==0 && res.toString().equals("")){

            }else{
                res.append(temp+"");
            }

            i++;
        }

        if(carry==0)
            return res.toString();
        else
            return "@";
    }

    public static int power(String A){
        int i=0;
        int l = A.length();
        while (l>0){
            A = divide(A);
            l = A.length();
            if(A.charAt(0)=='@')return 0;
            if(A.charAt(0)=='1' && l==1)return 1;

        }



        return 1;
    }

    public static void main(String[] args) {

//        System.out.println(power("29837912879371927397123791729319823971289739812937192739172379172937192739719827397129379127381273918273912873918273917293712937192379182970384059830485034895304850348503485034850834058304853827658763845762389456234"));
        System.out.println(power("344"));
        System.out.println(power("1"));
    }
}
