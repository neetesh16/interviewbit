package com.neetesh.InterviewBit.Strings;

import java.util.ArrayList;
import java.util.Collections;

public class MinimumCharactersStringPalindromic {

    public static ArrayList<Integer> computeLPS(String s) {
        ArrayList<Integer> lps = new ArrayList<>();
        int l = s.length();
        for (int i=0;i<l;i++){
            lps.add(0);
        }

        int i=1;
        int j = 0;
        while (i<l){
            if(s.charAt(i)==s.charAt(j)){
                j++;
                lps.set(i,j);
                i++;
            }else{
                if(j==0){
                    lps.set(i,0);
                    i++;
                }else{
                    j = lps.get(j-1);
                }
            }
        }
        return lps;
    }


    public static int isPalindrome(String A) {
        int len = A.length();
        int i = 0;
        String reverse = new StringBuffer(A).reverse().toString();
        reverse = A+"@"+reverse;
        ArrayList<Integer> lps = computeLPS(reverse);
        System.out.println(reverse);
        System.out.println(lps);
        return len - lps.get(lps.size()-1) ;
    }

    public static void main(String[] args) {

        System.out.println(isPalindrome("ABAACAABAA"));
        System.out.println(isPalindrome("AACECAAAA"));




    }
}
