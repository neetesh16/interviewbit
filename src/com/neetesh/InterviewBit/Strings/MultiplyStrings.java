package com.neetesh.InterviewBit.Strings;

public class MultiplyStrings {


    public static String addString(String A,String B){
        int alen = A.length();
        int blen = B.length();
        int i = 0;
        int j = 0;

        StringBuffer c = new StringBuffer("");
        int carry = 0;
        while (i<alen || j<blen){
            int tempAdd  = (i<alen?A.charAt(i):48)+(j<blen?B.charAt(j):48)-2*48+carry;
            carry = tempAdd/10;
            tempAdd = tempAdd%10;
            c.append(tempAdd+"");
            i++;j++;
        }
        while (carry!=0){
            c.append(carry%10+"");
            carry/=10;
        }
        return c.toString();
    }


    public static String multiply(String A, String B) {

        int alen = A.length();
        int blen = B.length();
        int i = alen-1;
        String ans = "";

        int k = 0;
        int mulcarry = 0;
        int addcarry = 0;
        StringBuffer x = new StringBuffer("");
        while (i>=0){
            int up = A.charAt(i)-48;
            int j = blen-1;
            mulcarry = 0;
            addcarry = 0;
            k = 0;

            StringBuffer c = new StringBuffer(x);
            while (j>=0){
                int down = B.charAt(j)-48;
                int mul = up*down+mulcarry;
                int num = mul%10;
                mulcarry = mul/10;
                c.append(num);

                j--;
            }
            while (mulcarry!=0){
                c.append(mulcarry%10+"");
                mulcarry/=10;
            }

            ans = addString(ans,c.toString());
            x.append("0");
            i--;
        }
        StringBuffer res = new StringBuffer();
        int lres = ans.length();
        int h = lres-1;
        while (h>0 && ans.charAt(h)=='0')h--;

        while (h>=0){
            res.append(ans.charAt(h));
            h--;
        }

        return res.toString();
    }

    public static void main(String[] args) {

        System.out.println(multiply("0","0"));
        System.out.println(multiply("23","79"));
    }
}
