package com.neetesh.InterviewBit.Strings;

public class PalindromeString {

    public static int check(char p) {
       if((p>='0' && p<='9') || (p>='a' && p<='z') || (p>='A' && p<='Z'))return 1;
       return 0;
    }


    public static int isPalindrome(String A) {
        int len = A.length()-1;
        int i = 0;
        A = A.toLowerCase();
        while (i<=len){
            if(check(A.charAt(i))==0 ){
                i++;continue;
            }
            if(check(A.charAt(len))==0 ){
                len--;continue;
            }
            if(A.charAt(i)==A.charAt(len)){
                i++;len--;
            }else{
                return 0;
            }
        }

        return 1;
    }

    public static void main(String[] args) {

        System.out.println(isPalindrome("A man, A plan,[]{]-)( A canal: Panama"));
        System.out.println(isPalindrome("  "));
        System.out.println(isPalindrome(""));
        System.out.println(isPalindrome("AAACA"));



    }
}
