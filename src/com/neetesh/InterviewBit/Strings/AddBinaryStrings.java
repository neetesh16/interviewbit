package com.neetesh.InterviewBit.Strings;

public class AddBinaryStrings {

    public static String addBinary(String A, String B) {
        int alen = A.length();
        int blen = B.length();
        int i = alen - 1;
        int j = blen - 1;
        int k = Math.max(alen,blen);
        int carry = 0;
        StringBuffer c = new StringBuffer("");
        while (k>0){
            int tempAdd = (i>=0?A.charAt(i):48)+(j>=0?B.charAt(j):48)+carry-2*48;
            carry = tempAdd/2;
            tempAdd = tempAdd%2;
            c.append(tempAdd+"");
            i--;j--;k--;
        }
        if(carry!=0){
            c.append(carry+"");
        }
        return  (c).reverse().toString();
    }

    public static void main(String[] args) {

        System.out.println(addBinary("100","11"));
        System.out.println(addBinary("10001","11101"));
    }
}
