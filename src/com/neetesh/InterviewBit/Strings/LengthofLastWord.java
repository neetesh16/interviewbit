package com.neetesh.InterviewBit.Strings;

public class LengthofLastWord {

    public static int lengthOfLastWord(final String A) {
        int i=0;
        int l = A.length();
        String temp = "";
        String ans = "";
        while (i<=l){
            if(i==l){
                if(!temp.equals("")){
                    ans = temp;
                    temp = "";
                }
                break;
            }
            if(A.charAt(i)==' '){
                if(!temp.equals("")){
                    ans = temp;
                    temp = "";
                }
            }else{
                temp+=A.charAt(i);
            }
            i++;
        }
        
        return ans.length();
    }
    
    public static void main(String[] args) {

        System.out.println(lengthOfLastWord("Hello World"));
        System.out.println(lengthOfLastWord("  "));
        System.out.println(lengthOfLastWord(""));
        System.out.println(lengthOfLastWord("AAACA  "));



    }
}
