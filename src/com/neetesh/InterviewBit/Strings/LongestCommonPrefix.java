package com.neetesh.InterviewBit.Strings;

import java.util.ArrayList;
import java.util.Arrays;

public class LongestCommonPrefix {


    public static String longestCommonPrefix(ArrayList<String> A) {
        String res = "";
        int l = A.size();
        int minLen = Integer.MAX_VALUE - 1;
        for (int i = 0; i < l; i++) {
            minLen = Math.min(minLen, A.get(i).length());
        }
        for (int j = 0; j < minLen; j++) {
            int flag = 0;
            for (int i = 1; i < l; i++) {
                if (A.get(i).charAt(j) != A.get(i - 1).charAt(j)) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                res += A.get(0).charAt(j);
            } else {
                return res;
            }

        }
        return res;
    }

    public static void main(String[] args) {

        String[] s = new String[]{"abfdefgh", "abfghijk", "abf"};
        System.out.println(longestCommonPrefix(new ArrayList<>(Arrays.asList(s))));

        s = new String[]{"abfdefgh", "abfghijk", "abf"};
        System.out.println(longestCommonPrefix(new ArrayList<>(Arrays.asList(s))));

        s = new String[]{"abcdefgh", "abcdfefgh"};
        System.out.println(longestCommonPrefix(new ArrayList<>(Arrays.asList(s))));

        s = new String[]{""};
        System.out.println(longestCommonPrefix(new ArrayList<>(Arrays.asList(s))));

    }
}
