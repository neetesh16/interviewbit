package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;
import java.util.Collections;

public class NobleInteger {

    public static int solve(ArrayList<Integer> A){
        Collections.sort(A);
        int l = A.size();
        for (int i=0;i<A.size();i++){
            if(i<l-1 && A.get(i)==A.get(i+1)){
                continue;
            }
            if(A.get(i)==l-i-1){
                return 1;
            }
        }
        return -1 ;
    }

    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<Integer>();

        A.clear();
        A.add(10);

        A.add(20);
        A.add(40);
        A.add(2);
        A.add(1);
        System.out.println(solve(A));
    }
}
