package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class WaveArray {

    public static ArrayList<Integer> wave(ArrayList<Integer> a){
        Collections.sort(a);
        for(int i=1;i<a.size();i=i+2){
            if(i>0){
                if(a.get(i) > a.get(i-1)){
                    int t = a.get(i);
                    a.set(i,a.get(i-1));
                    a.set(i-1,t);
                }
            }

            if(i<a.size()-1){
                if(a.get(i) > a.get(i+1)){
                    int t = a.get(i);
                    a.set(i,a.get(i+1));
                    a.set(i+1,t);
                }
            }
        }

        return a;
    }

    public static void main(String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(10,20,40,2,1));
        System.out.println(wave(a));
        a = new ArrayList<Integer>(Arrays.asList(10,20));
        System.out.println(wave(a));
        a = new ArrayList<Integer>(Arrays.asList(5, 1, 3, 2, 4));
        System.out.println(wave(a));
    }
}
