package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;

public class MinStepsinInfiniteGrid {


    public static int coverPoints(ArrayList<Integer> X, ArrayList<Integer> Y){
        int res = 0;
        for(int i =1; i<X.size();i++){
            res = Math.max(Math.abs(X.get(i)-X.get(i-1)),Math.abs(Y.get(i)-Y.get(i-1)));
        }
        return res;

    }

    public static void main(String[] args){
        ArrayList<Integer> X = new ArrayList<Integer>();
        ArrayList<Integer> Y = new ArrayList<Integer>();

        X.add(-7);
        X.add(1);
        //X.add(1);

        Y.add(-13);
        //Y.add(1);
        Y.add(-5);
        System.out.println(coverPoints(X, Y));

    }
}
