package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;

public class MaxNonNegativeSubArray {
    public static ArrayList<Integer> maxset(ArrayList<Integer> A){
        int start = 0;
        int maxStart = -1;
        int maxEnd = -1;
        long maxSum = -1;
        long currSum = 0;

        for (int i=0;i<A.size();i++){
            if(A.get(i) >= 0){
                currSum+=A.get(i);
                if(i==A.size()-1){
                    if(currSum > maxSum){
                        maxSum = currSum;
                        maxStart = start;
                        maxEnd = i;
                    }else if(currSum == maxSum){
                        if(maxEnd-maxStart < i-start ){
                            maxEnd = i;
                            maxStart = start;
                        }
                    }
                }
            }else{
                if(currSum > maxSum){
                    maxSum = currSum;
                    maxStart = start;
                    maxEnd = i-1;
                }else if(currSum == maxSum){
                    if(maxEnd-maxStart < i-start ){
                        maxEnd = i-1;
                        maxStart = start;
                    }
                }

                currSum = 0;
                start = i+1;

            }
            System.out.println("Array:- "+currSum);

        }
//        System.out.println("Max Sum:- "+maxSum);
//        System.out.println("Array:- "+maxStart+" "+maxEnd);
//        System.out.println("Array:- "+A.subList(maxStart,maxEnd+1));

        return new ArrayList<>(A.subList(maxStart,maxEnd+1)) ;

    }

    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<Integer>();
//        A.add(5);
//        A.add(2);
//        A.add(5);
//        A.add(-7);
//        A.add(2);
//        A.add(10);
//
//        System.out.println(maxset(A));
//        A.clear();
//        A.add(-1);
//
//
//        System.out.println(maxset(A));

        A.clear();
        A.add(1967513926);
        A.add(1540383426);
        A.add(-1303455736);
        A.add(-521595368 );
        System.out.println(maxset(A));
    }
}
