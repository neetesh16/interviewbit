package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MergeIntervals {
    static class  Interval{
      public int start;
      public int end;

        public Interval(int start, int end) {
            this.start = start;
            this.end = end;
        }
    };

    public static boolean isMergePosible(Interval a,Interval b){
        if((a.start<=b.start && b.start<=a.end) || (b.start<=a.start && a.start<=b.end) )
             return true;
        return false;
    }
    public static ArrayList<Interval> insert(ArrayList<Interval> intervals, Interval newInterval) {
        ArrayList<Interval> res = new ArrayList<>();
        int len = intervals.size();
        boolean used = false;
        Interval prev = new Interval(-1,-1) ;
        for(int i=0;i<len;i++){
            Interval curr = intervals.get(i);
            if(!used ){
                if(isMergePosible(newInterval,curr)){
                    curr.start = Math.min(curr.start,newInterval.start);
                    curr.end = Math.max(curr.end,newInterval.end);
                    used = true;
                }else{

                }

            }

            if(i>0){
                if(isMergePosible(prev,curr)){
                    prev.start = Math.min(curr.start,prev.start);
                    prev.end = Math.max(curr.end,prev.end);

                }else{
                    res.add(prev);
                    prev = curr;
                    if(!used && curr.start > newInterval.end){
                        res.add(newInterval);
                        used = true;
                    }
                }
            }else{
                prev = curr;
                if(!used && curr.start > newInterval.end){
                    res.add(newInterval);
                    used = true;
                }

            }

        }
        if(len!=0)
            res.add(prev);
        if(!used)res.add(newInterval);
//        for(int i=0;i<res.size();i++) {
//            System.out.println(res.get(i).start+" "+res.get(i).end);
//        }

        return res;
    }

    public static void main(String[] args) {
        ArrayList<Interval> intervals = new ArrayList<>();

        intervals.add(new Interval(1,2));
        intervals.add(new Interval(3,5));
        intervals.add(new Interval(6,7));
        intervals.add(new Interval(8,10));
        intervals.add(new Interval(12  ,16));
        insert(intervals,new Interval(4,9));
        System.out.println("");
        intervals.clear();


        insert(intervals,new Interval(1,1));

        System.out.println("");
        intervals.clear();
        intervals.add(new Interval(1,2));
        intervals.add(new Interval(8,10));

        insert(intervals,new Interval(3,6));



        System.out.println("");
        intervals.clear();
        intervals.add(new Interval(3,5));
        intervals.add(new Interval(8,10));

        insert(intervals,new Interval(1,2));

        System.out.println("");
        intervals.clear();
        intervals.add(new Interval(3,5));
        intervals.add(new Interval(8,10));

        insert(intervals,new Interval(11,12));
    }
}
