package com.neetesh.InterviewBit.Arrays;

import java.util.*;

public class SetMatrixZeros {

    public static void setZeroes(ArrayList<ArrayList<Integer>> a) {
        if(a.size()==0)return;
        boolean[] rows = new boolean[a.size()];
        boolean[] columns = new boolean[a.get(0).size()];
        for (int i=0;i<a.size();i++){
            for (int j=0;j<a.get(i).size();j++){
                if(a.get(i).get(j)==0){
                    rows[i] = true;
                    columns[j] = true;
                }
            }

        }
        int k = a.get(0).size();
        for (int i=0;i<a.size();i++){
            for (int j=0;j<k;j++){
                if(rows[i]==true || columns[j]==true){
                   a.get(i).set(j,0);
                }
            }
        }

    }

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> a = new ArrayList<>();
        Integer[] a1 ;
         a1 = new Integer[]{1,0,1,0};
        a.add(new ArrayList<>(Arrays.asList(a1)));
         a1 = new Integer[]{1,1,1,1};
        a.add(new ArrayList<>(Arrays.asList(a1)));
         a1 = new Integer[]{1,1,1,1};
        a.add(new ArrayList<>(Arrays.asList(a1)));
        setZeroes(a);
        for (int i=0;i<a.size();i++){
            for (int j=0;j<a.get(i).size();j++){
                System.out.print(a.get(i).get(j)+" ");
            }
            System.out.println("");
        }
        System.out.println();
    }
}
