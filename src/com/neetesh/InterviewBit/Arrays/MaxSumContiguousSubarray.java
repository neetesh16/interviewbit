package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class MaxSumContiguousSubarray {


    public static int maxSubArray(ArrayList<Integer> A) {
        int result = 0;
        int k = A.size();
        if(k>0)result = A.get(0);
        int current = A.get(0);
        for(int i=1;i<k;i++){
            current = Math.max(current+A.get(i),A.get(i));
           result = Math.max(result,current);
        }
        return result;

    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
         a = new Integer[]{-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(maxSubArray(new ArrayList<>(Arrays.asList(a))));

//        a = new Integer[]{1, 2, 0};;
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{3,4,-1,1};;
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{-8, -7, -6};;
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{1,2,3};
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

    }
}
