package com.neetesh.InterviewBit.Arrays;

import java.util.*;

public class LargestNumber {

    public static String largestNumber(final List<Integer> a) {
        Collections.sort(a, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                String s1 = o1+"";
                String s2 = o2+"";
                if((s1.concat(s2)).compareTo(s2.concat(s1))>0)return -1 ;
                return 1;
            }
        });
        String res = "";
        ArrayList<String>B = new ArrayList<>();
        for (int x:a ){
            res+=x+"";
            B.add(Integer.toString(x));
        }
        Collections.sort(B, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {

                return (s1.concat(s2)).compareTo(s2.concat(s1));
            }
        });
        String res2 = "";
        for (int k = B.size()-1;k>=0;k--)res2+=B.get(k)+"";


        System.out.println("res2 "+res2);
        int i;
        for(i = 0; i < res.length() && res.charAt(i) == '0'; ++i);
        if(i==res.length())return "0";
        return res.substring(i);

    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
         a = new Integer[]{3, 30, 34, 5, 9};;
        System.out.println(largestNumber(new ArrayList<>(Arrays.asList(a))));

         a = new Integer[]{0,3,0};
        System.out.println(largestNumber(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{54,546,548,60};
        System.out.println(largestNumber(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{0,03,0,0};
        System.out.println(largestNumber(new ArrayList<>(Arrays.asList(a))));
    }
}
