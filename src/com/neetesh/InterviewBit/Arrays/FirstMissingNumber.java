package com.neetesh.InterviewBit.Arrays;

import java.util.*;

public class FirstMissingNumber {

    public static int firstMissingPositive(ArrayList<Integer> A) {
        int len = A.size();
        for(int i=0;i<len;i++){
            if(A.get(i)<=0)A.set(i,0);
        }
        System.out.println(A);
        for(int i=0;i<len;i++){
//            System.out.println(i);
//            System.out.println(A);
            int p = A.get(i);
            if(p<=len && p>=(-1*len)){
                if(p!=0){
                    p = Math.abs(p)-1;
                    if(A.get(p)==0){
                        A.set(p,-1*(p+1));
                    }else{
                        A.set(p,-1*Math.abs(A.get(p)));
                    }

                }else{

                }
            }
        }
        int i=0;
        System.out.println(A);
        for(i=0;i<len;i++){

            if(A.get(i)>=0)break;
        }



        return i+1;

    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
         a = new Integer[]{3, 30, 34, 5, 9};;
        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{1, 2, 0};;
        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{3,4,-1,1};;
        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{-8, -7, -6};;
        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

        a = new Integer[]{1,2,3};
        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

    }
}
