package com.neetesh.InterviewBit.Arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class AddOneToNumber {


    public static ArrayList<Integer> plusOne(ArrayList<Integer> A) {
        ArrayList<Integer> result = new ArrayList<>();
        int k = A.size();
        int add = 1;
        for(int i=k-1;i>=0;i--){
            int temp =(A.get(i)+add);
            A.set(i,temp%10);
            add = temp/10;
        }
        System.out.println(A);
        if(add!=0){
            A.add(0,add);
            k++;
        }
        System.out.println(A);
        int i = 0;
        while (A.get(i)==0)i++;
        for( ;i<k;i++){
            result.add(A.get(i));
        }
        return result;

    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
         a = new Integer[]{9, 9, 9, 9, 9};
        System.out.println(plusOne(new ArrayList<>(Arrays.asList(a))));

//        a = new Integer[]{1, 2, 0};;
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{3,4,-1,1};;
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{-8, -7, -6};;
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));
//
//        a = new Integer[]{1,2,3};
//        System.out.println(firstMissingPositive(new ArrayList<>(Arrays.asList(a))));

    }
}
