package com.neetesh.InterviewBit.Bits;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SingleNumber {
    public static int singleNumber(final List<Integer> A) {
        int l = A.size();
        if(l==0)return 0;
        int res = A.get(0);
        int i = 1;
        while (i<l){
            res = res^A.get(i);
            i++;
        }
        return res;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<>(Arrays.asList(new Integer[]{0, 2 ,5 ,7 }));
        out(singleNumber(A));
    }

}
