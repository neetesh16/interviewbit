package com.neetesh.InterviewBit.Bits;

import java.util.ArrayList;
import java.util.Arrays;

public class DifferentBitsSumPairwise {
    private static long reverse(ArrayList<Integer> A) {
        long MOD = 1000000007;
        long len = A.size();
        long result = 0L;
        long p = 1L;
        for(int i=0;i<32;i++){

            long c = 0;
            for(int j=0;j<len;j++){
                long temp = A.get(j);
                if( (temp & p) ==0){
//                    System.out.println(((1L<<i)) +" "+p);
                    c++;
                }
            }
            p = p*2L;
            result = result%MOD +  (2L*c*(len-c))%MOD;
            result = result%MOD;
        }
        int ans = (int)(result);
        return ans;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        Integer[] a = new Integer[]{3, 30, 34, 5, 9};
        a = new Integer[]{4, 0, 2, 1, 3};
        ArrayList<Integer> p = new ArrayList<>(Arrays.asList(a));
        out(reverse(p));
    }

}
