package com.neetesh.InterviewBit.Bits;

public class ReverseBits {
    private static long reverse(long n) {
//        System.out.println(String.format("%32s", Long.toBinaryString(n)).replace(' ', '0'));
        long rev = 0;
        int count = 0;
        while (n!=0){
            if((n & 1)==1)
                rev = (1L << 32-count-1) | rev;
            count++;
            n = n >> 1;
        }
//        System.out.println(String.format("%32s", Long.toBinaryString(rev)).replace(' ', '0'));
        return rev;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        out(reverse(3));
        out(reverse(0));
        out(reverse(1));
    }

}
