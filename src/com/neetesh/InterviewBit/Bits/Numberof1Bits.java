package com.neetesh.InterviewBit.Bits;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Numberof1Bits {
    public static int numSetBits(long a) {
        int res = 0;
        while (a!=0){
            if((a & 1)==1L){
                res++;
            }
            a = a>>1;
        }
        return res;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        out(numSetBits(11));
    }

}
