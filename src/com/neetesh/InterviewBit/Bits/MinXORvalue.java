package com.neetesh.InterviewBit.Bits;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MinXORvalue {
    public static int findMinXor(ArrayList<Integer> A) {
        int l = A.size();
        Collections.sort(A);
        int res = Integer.MAX_VALUE-1;
        for (int i=1;i<l;i++){
            int temp = A.get(i)^A.get(i-1);
            res = Math.min(res,temp);
        }
        return res;
    }

    static void  out(Object l){
        System.out.println(l);
    }
    public static void main(String[] args) {
        ArrayList<Integer> A = new ArrayList<>(Arrays.asList(new Integer[]{0, 2 ,5 ,7 }));
        out(findMinXor(A));
    }

}
