package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class SimplifyDirectoryPath {

    public static String simplifyPath(String A) {
        Deque<String> s = new LinkedList<>();
        StringBuilder res = new StringBuilder("");
        String[] paths = A.split("/");
        int len = paths.length;
        int i=0;
        while (i<len){
            if(paths[i].equals("") || paths[i].equals(".")){
                i++;continue;
            }else  if(paths[i].equals("..")){
                if(!s.isEmpty())s.removeLast();

            }else{
                s.addLast(paths[i]);
            }
            i++;
        }
        if(s.isEmpty()){
            return "/";
        }
        while (!s.isEmpty()){
            res.append("/"+s.getFirst()) ;
            s.removeFirst();
        }
        return  res.toString();
    }

    public static void main(String[] args) {
//        String a = "/home/";
//        System.out.println(simplifyPath(a));
//        System.out.println(simplifyPath("/a/./b/../../c/"));
//        System.out.println(simplifyPath("/a/./"));
//        System.out.println(simplifyPath("/a/.."));
//        System.out.println(simplifyPath("/a/../"));
//        System.out.println(simplifyPath("/../../../../../a"));
//        System.out.println(simplifyPath("/a/./b/./c/./d/"));
//        System.out.println(simplifyPath("/a/../.././../../."));
        System.out.println(simplifyPath("/./.././ykt/xhp/nka/eyo/blr/emm/xxm/fuv/bjg/./qbd/./../pir/dhu/./../../wrm/grm/ach/jsy/dic/ggz/smq/mhl/./../yte/hou/ucd/vnn/fpf/cnb/ouf/hqq/upz/akr/./pzo/../llb/./tud/olc/zns/fiv/./eeu/fex/rhi/pnm/../../kke/./eng/bow/uvz/jmz/hwb/./././ids/dwj/aqu/erf/./koz/.."));

    }
}
