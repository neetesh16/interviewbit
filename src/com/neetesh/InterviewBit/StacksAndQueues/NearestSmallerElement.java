package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class NearestSmallerElement {

    public static ArrayList<Integer> prevSmaller(ArrayList<Integer> A) {
        ArrayList<Integer> res = new ArrayList<>();
        Stack<Integer>s = new Stack<>();
        int len = A.size();
        int i = 0;
        while (i<len){
            int element = A.get(i);
            while (!s.empty() && s.peek()>=element){
                s.pop();
            }
            if(s.empty()){
                res.add(-1);
            }else{
                res.add(s.peek());
            }
            s.push(element);
            i++;
        }
        return res;
    }

    public static void main(String[] args) {
        Integer[] a = new Integer[]{4, 5, 2, 10, 8};
        System.out.println(prevSmaller(new ArrayList<Integer>(Arrays.asList(a))));
        System.out.println(prevSmaller(new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 6, 4, 10, 2, 5}))));
        System.out.println(prevSmaller(new ArrayList<Integer>(Arrays.asList(new Integer[]{3,2,1}))));
        System.out.println(prevSmaller(new ArrayList<Integer>(Arrays.asList(new Integer[]{1,2,3}))));
        System.out.println(prevSmaller(new ArrayList<Integer>(Arrays.asList(new Integer[]{ 39, 27, 11, 4, 24, 32, 32, 1}))));

    }
}
