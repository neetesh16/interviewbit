package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.*;

public class SlidingWindowMaximum {

    public static ArrayList<Integer> slidingMaximum(final List<Integer> A, int B) {

        ArrayList<Integer> res = new ArrayList<>();
        int len = A.size();
        int i = 0;

        Deque queue = new LinkedList();
        int p = Math.min(len, B);
        while (i < len) {
            if (queue.isEmpty()) {
                while (p > 0) {
                    while (!queue.isEmpty() && A.get((Integer) queue.getLast()) <= A.get(i)) {
                        queue.removeLast();
                    }
                    queue.add(i);
                    p--;
                    i++;
                }
//                System.out.println("i " + A.get((Integer) queue.getFirst()));
                res.add(A.get((Integer) queue.getFirst()));
            } else {
                int l = i - B;
                while (!queue.isEmpty() && (Integer) queue.getFirst() <= l) {
                    queue.removeFirst();
                }

                while (!queue.isEmpty() && A.get((Integer) queue.getLast()) <= A.get(i)) {
                    queue.removeLast();
                }
                queue.add(i);

                i++;
//                System.out.println("i " + A.get((Integer) queue.getFirst()));
                res.add(A.get((Integer) queue.getFirst()));
            }

        }
        return res;
    }

    public static void main(String[] args) {
//        String[] a = new String[]{"2", "1", "+", "3", "*"};
//        System.out.println(evalRPN(new ArrayList<Integer>(Arrays.asList(a))));
        System.out.println(slidingMaximum(new ArrayList<Integer>(Arrays.asList(new Integer[]{1, 3, -1, -3, 5, 3, 6, 7})), 1));

    }
}
