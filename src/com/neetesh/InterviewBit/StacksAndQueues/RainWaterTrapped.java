package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class RainWaterTrapped {
    public static int trap(final List<Integer> A) {
        int res = 0;
        Stack<Integer> s = new Stack<>();
        int len = A.size();
        int i = 0;
        while (i<len){
            if(s.empty() || A.get(s.peek())>=A.get(i)){
                s.push(i);
                i++;
            }else{
                int top = s.peek();
                s.pop();
                int left = 0;
                int right = i;
                if(!s.empty()){
                    left = s.peek() ;
                    int area = (i-left-1)*(Math.min(A.get(i),A.get(left))-A.get(top));
                    res+= area;
                }

            }
        }

        return res;
    }

    public static void main(String[] args) {
        System.out.println(trap(new ArrayList<Integer>(Arrays.asList(new Integer[]{0,1,0,2,1,0,1,3,2,1,2,1}))));
        System.out.println(trap(new ArrayList<Integer>(Arrays.asList(new Integer[]{2,0,2}))));
        System.out.println(trap(new ArrayList<Integer>(Arrays.asList(new Integer[]{3, 0, 0, 2, 0, 4}))));

    }
}
