package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class LargestRectangleinHistogram {
    public static int largestRectangleArea(ArrayList<Integer> A) {
        int res = 0;
        Stack<Integer> s = new Stack<>();
        int len = A.size();
        int i = 0;
        while (i<len){
            if(s.empty() || A.get(s.peek())<=A.get(i)){
                s.push(i);
                i++;
            }else{
                int top = s.peek();
                s.pop();
                int left = 0;
                int right = i;
                if(!s.empty()){
                    left = s.peek() +1;
                }
                int area = A.get(top)*( right - left);
                res = Math.max(res,area);
            }
        }

        while (!s.empty()){
            int top = s.peek();
            s.pop();
            int left = 0;
            int right = i;
            if(!s.empty()){
                left = s.peek() +1;
            }
            int area = A.get(top)*(right - left);
            res = Math.max(res,area);
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(largestRectangleArea(new ArrayList<Integer>(Arrays.asList(new Integer[]{6, 2, 5, 4, 5, 1, 6}))));
        System.out.println(largestRectangleArea(new ArrayList<Integer>(Arrays.asList(new Integer[]{2,1,5,6,2,3}))));

    }
}
