package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.*;

public class RedundantBraces {

    public static int braces(String A) {
        Stack<Character>s = new Stack<>();
        int res = 0;
        int len = A.length();
        int i =0 ;
        while (i<len){
            if(A.charAt(i)==')'){
                if(s.peek()=='(')return 1;
                else{
                    int count = 0;
                    while (!s.empty()){
                        if(s.peek()=='('){
                            s.pop();
                            break;
                        }else{
                            count++;
                            s.pop();
                        }
                    }
                    if(count<=1)return 1;
                }

            }else{
                s.push(A.charAt(i));
            }

            i++;
        }


        return  res;
    }

    public static void main(String[] args) {
        String a = "(a)";
        System.out.println(braces(a));

    }
}
