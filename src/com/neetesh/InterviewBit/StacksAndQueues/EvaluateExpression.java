package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class EvaluateExpression {

    public static int evalRPN(ArrayList<String> A) {
        int res = 0;
        Stack<String>s = new Stack<>();
        int len = A.size();
        int i = 0;
        while (i<len){
            String element = A.get(i);
            if(element.equals("/")){
                String second = s.pop();
                String first = s.pop();
                int p = Integer.parseInt(first)/Integer.parseInt(second);
                s.push(p+"");

            }else if(element.equals("+")){
                String second = s.pop();
                String first = s.pop();
                int p = Integer.parseInt(first)+Integer.parseInt(second);
                s.push(p+"");

            }else if(element.equals("-")){
                String second = s.pop();
                String first = s.pop();
                int p = Integer.parseInt(first)-Integer.parseInt(second);
                s.push(p+"");

            }else if(element.equals("*")){
                String second = s.pop();
                String first = s.pop();
                int p = Integer.parseInt(first)*Integer.parseInt(second);
                s.push(p+"");

            }else{
                s.push(element);
            }
            i++;
        }
        return Integer.parseInt(s.pop());
    }

    public static void main(String[] args) {
        String[] a = new String[]{"2", "1", "+", "3", "*"};
        System.out.println(evalRPN(new ArrayList<String>(Arrays.asList(a))));
        System.out.println(evalRPN(new ArrayList<String>(Arrays.asList(new String[]{"4", "13", "5", "/", "+"}))));

    }
}
