package com.neetesh.InterviewBit.StacksAndQueues;

import java.util.Stack;

public class MinStack {
    public Stack<Integer> s;
    public Stack<Integer> min;

    public MinStack() {
        s = new Stack<>();
        min = new Stack<>();
    }

    public void push(int x){
        s.push(x);
        if(min.isEmpty() || min.peek()>=x){
            min.push(x);
        }
    }

    public void pop(){
        if(!s.isEmpty()){
            int ele = s.pop();
            if(!min.isEmpty() && min.peek()==ele){
                min.pop();
            }
        }

    }

    public int top() {
        if(s.isEmpty())
            return -1;
        return s.peek();
    }

    public int getMin() {
        if(min.isEmpty())
            return -1;
        return min.peek();
    }

    public static void main(String[] args) {
        MinStack s = new MinStack();
        s.push(6);
        s.push(3);
        s.push(9);
        s.push(5);
        s.push(1);
        s.push(1);
        s.push(2);
        System.out.println(s.s);
        System.out.println(s.min);

        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);

        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);

        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);

        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);

        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);

        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);
        s.pop();
        System.out.println(s.s);
        System.out.println(s.min);
        System.out.println(s.top());
        System.out.println(s.getMin());


    }
}
