package com.neetesh.InterviewBit.LinkedList;

public class ReorderList {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode reorderList(ListNode A) {
        ListNode current = A;
        ListNode end = null;
        ListNode fastPtr = current;
        ListNode middleHalf = current;
        ListNode previousMiddle = null;
        int count = 1;
        while (fastPtr!=null && fastPtr.next!=null){
            fastPtr = fastPtr.next.next;
            previousMiddle = middleHalf;
            count++;
            middleHalf = middleHalf.next;
        }
        if(count==1)return A;
//        out("count "+count);
//        out(middleHalf.val);
//        out(previousMiddle.val);

        current = middleHalf;
        ListNode previous = null;
        while (current!=null){
            ListNode next = current.next;
            current.next = previous;
            previous = current;
            current = next;
        }

        previousMiddle.next = previous;
        current = A;
//        printList(A);
        ListNode currentOther = previous;
        while (current.next!=currentOther){
            ListNode firstHalfNext = current.next;
            ListNode secondHalfNext = currentOther.next;

            current.next = currentOther;
            currentOther.next = firstHalfNext;

            previousMiddle.next = secondHalfNext;

            current = firstHalfNext;
            currentOther = secondHalfNext;

            count--;
        }
        return A;
    }


    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{1};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


        printList(A);
        A = reorderList( A);
        printList(A);
//        printList(A);

    }


}
