package com.neetesh.InterviewBit.LinkedList;

import java.util.HashMap;

public class ListCycle {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        HashMap<Integer,Integer> check= new HashMap<>();
        ListNode current = head;
        while (current != null) {

            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            if(check.containsKey(current.val)){
                break;
            }else{
                check.put(current.val,1);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode detectCycle(ListNode A) {
        ListNode current = null;
        
        ListNode fastPtr = A;
        ListNode slowPtr = A;
        while (slowPtr.next!=null && fastPtr.next.next!=null){

            fastPtr = fastPtr.next.next;
            slowPtr = slowPtr.next;
            if(fastPtr==slowPtr){
                current = slowPtr;
                break;
            }

        }
        if (current==null)return null;
        slowPtr = A;
        while (slowPtr!=current){
            slowPtr = slowPtr.next;
            current = current.next;
        }
        return current;
    }

    public static boolean hasCycle(ListNode head) {
        if(head==null) return false;
        ListNode slow = head;
        ListNode fast = head;
        boolean isCycle = false;
        while(slow.next != null && fast.next.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if(slow == fast) {
                isCycle = true;
                break;
            }
        }
        if (!isCycle)
            return false;
        fast = head;

        while (slow != fast) {
            slow = slow.next;fast = fast.next;
        }
        out(fast);
        return true;
    }
    
    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{87797 ,23219 ,41441 ,58396 ,48953, 94603, 2721, 95832, 49029 ,98448, 65450};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }
        int idx = 2;
        ListNode end = null;
        ListNode current = A;
        ListNode ListNode = null;
//        while (current.next!=null){
//            if(idx==0){
//                ListNode = current;
//            }
//            current = current.next;
//            idx--;
//        }
//        current.next = ListNode;


        printList(A);
        A = detectCycle( A);
//        hasCycle(A);
//        printList(A);
//        printList(A);

    }


}
