package com.neetesh.InterviewBit.LinkedList;


public class AddTwoNumbersLists {

    static class ListNode {
        int val;
        ListNode  next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val,ListNode head){
        ListNode n = new ListNode(val);
        if (head == null){
            head = new ListNode(val);
        }else{
            ListNode current = head;
            while (current.next!=null)current=current.next;
            current.next = n;
        }
    }

    public static ListNode addTwoNumbers(ListNode A,ListNode B){
        ListNode C = null;
        ListNode start = null;
        ListNode a = A;
        ListNode b = B;
        int carry = 0;
        while (a!=null || b!=null){
            int temp = (a!=null?a.val:0) + (b!=null?b.val:0) + carry;
            ListNode t = new ListNode(temp%10);
            carry = temp/10;
            if (C==null){
               C = t;
               start = t;
            }else{
               C.next = t ;
               C = C.next;
            }
            if(a!=null)
                a = a.next;
            if(b!=null)
                b = b.next;
        }
        if (carry!=0){
            C.next  = new ListNode(carry);
        }

        a = null;
        b = start;
        while (b!=null){
            ListNode next = b.next;
            b.next = a;
            a = b;
            b = next;
        }

        while (a!=null && a.val==0){
            a = a.next;
        }
        if(a==null){
            return new ListNode(0);
        }

        b = a;
        a = null;
        while (b!=null){
            ListNode next = b.next;
            b.next = a;
            a = b;
            b = next;
        }
        return a;
    }

    private static void printList(ListNode head) {
        ListNode current = head;
        while (current!=null){
            if(current==head){
                System.out.print(current.val);
            }else{
                System.out.print(" -> "+current.val);
            }

            current=current.next;
        }
        System.out.println("");
    }
    private static void out(Object o) {
        System.out.println(o);
    }
    public static void main(String[] args) {
        ListNode A = null;
        ListNode B = null;
        Integer []a = new Integer[]{0,0,0,0};
        Integer []b = new Integer[]{0,0};
        for(Integer x: a){
            if(A==null)A = new ListNode(x);
            else add(x,A);
        }
        for(Integer x: b){
            if(B==null)B = new ListNode(x);
            else add(x,B);
        }

        printList(A);
        printList(B);
        A = addTwoNumbers(A,B);
        printList(A);
//        printList(B);

    }



}
