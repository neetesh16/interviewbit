package com.neetesh.InterviewBit.LinkedList;

public class SortList {
    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    
    public static ListNode sortList(ListNode A) {

        if(A==null || A.next==null)return A;
        ListNode fast = A;
        ListNode slow = A;
        ListNode previous = A;
        if(A!=null){
            while (fast!=null && fast.next!=null){
                fast = fast.next.next;
                previous = slow;
                slow = slow.next;
            }
        }
        previous.next = null;
        A = sortList(A);
        slow = sortList(slow);
        A = mergeTwoLists(A,slow);

        
        return A;
    }

    public static ListNode mergeTwoLists(ListNode A, ListNode B){
        ListNode start = new ListNode(-1);
        ListNode currentA = A ;
        ListNode currentB = B;
        if(currentA == null && currentB==null)return A;
        if(currentA == null && currentB!=null)return B;
        if(currentA != null && currentB==null)return A;

        start = null;
        ListNode ptr = null;
        while (currentA!=null && currentB!=null){
            if (currentA.val<currentB.val){
                if (start == null){
                    start = currentA;
                    ptr = currentA;
                }else{
                    ListNode temp = currentA;

                    start.next = temp;
                    start = start.next;
                }
                currentA = currentA.next;
            }else{
                if (start == null){
                    start = currentB;
                    ptr = currentB;
                }else{
                    ListNode temp = currentB;

                    start.next = temp;
                    start = start.next;
                }
                currentB = currentB.next;
            }

        }

        while (currentA!=null){
            start.next = currentA;
            currentA = currentA.next;
            start = start.next;
        }
        while (currentB!=null){
            start.next = currentB;
            currentB = currentB.next;
            start = start.next;
        }
        return ptr;

    }

    public static void main(String[] args) {

        ListNode A = null;
        Integer[] b = new Integer[]{5,4,3,2,1};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


        printList(A);
        A = sortList( A);
        printList(A);
//        printList(A);

    }



}
