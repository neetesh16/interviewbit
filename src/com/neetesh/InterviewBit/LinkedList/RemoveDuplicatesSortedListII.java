package com.neetesh.InterviewBit.LinkedList;

public class RemoveDuplicatesSortedListII {
    static ListNode head;
    static class ListNode {
        int val;
        ListNode  next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val){
        ListNode n = new ListNode(val);
        if (head == null){
            head = n;
        }else{
            ListNode current = head;
            while (current.next!=null)current=current.next;
            current.next = n;
        }
    }

    public static ListNode deleteDuplicates(ListNode A){
        ListNode current = A;
        ListNode previous = null;
        while (current!=null && current.next!=null){
            ListNode next = current.next;
            if(next.val==current.val){
                while (next!=null && next.val==current.val){
                    next = next.next;
                }
            }else{
                previous = current;
            }

            if(previous == null){
                A = next;
                current.next = null;

            }else{
                previous.next = next;
            }
            current = next;

        }
        return A;

    }

    private static void printList() {
        ListNode current = head;
        while (current!=null){
            if(current==head){
                System.out.print(current.val);
            }else{
                System.out.print(" -> "+current.val);
            }

            current=current.next;
        }
        System.out.println("");
    }
    private static void out(Object o) {
        System.out.println(o);
    }
    public static void main(String[] args) {
        Integer []a = new Integer[]{1,1,1,2,3,3,4,4,5,6};
        for(Integer x: a){
            add(x);
        }


//        printList();
        head = deleteDuplicates(head);
        printList();

    }



}
