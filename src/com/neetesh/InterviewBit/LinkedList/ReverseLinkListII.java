package com.neetesh.InterviewBit.LinkedList;

public class ReverseLinkListII {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode reverseBetween(ListNode A,int B,int C) {
        ListNode current = A;
        ListNode previous = null;
        ListNode start = null;
        ListNode listPrev = null;
        ListNode next = null;
        int count = 0;
        while (current != null) {
            count++;
            if (count<B){
                previous = current;
                current = current.next;
            }
            if (count>=B && count<=C){
                if(start==null) {
                    start = current;
                }
                next = current.next;
                current.next = listPrev;
                listPrev = current;
                current = next;
            }
            if(count>C){
                break;
            }
        }

        if(previous == null){
            start.next = next;
            previous = listPrev;
            A = listPrev;
        }else{
            start.next = next;
            previous.next = listPrev;
        }

        return A;
    }


    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{1,2,3,4,5,6,7,8,9,10};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


        printList(A);
        A = reverseBetween( A,4,10);
        printList(A);
//        printList(A);

    }


}
