package com.neetesh.InterviewBit.LinkedList;

import java.util.List;

public class PalindromeList {
    static ListNode head;
    static class ListNode {
        int val;
        ListNode  next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val){
        ListNode n = new ListNode(val);
        if (head == null){
            head = n;
        }else{
            ListNode current = head;
            while (current.next!=null)current=current.next;
            current.next = n;
        }
    }

    public static int lPalin(ListNode A){
        ListNode fast = A;
        ListNode slow = A;
        ListNode previous = A;
        if(A!=null){
            while (fast!=null && fast.next!=null){
                fast = fast.next.next;
                previous = slow;
                slow = slow.next;
            }
        }
//        out(slow.val);
//        out(previous.val);

        ListNode current = slow;
        ListNode last = null;
        while (current!=null){
            ListNode temp = current.next;
            current.next = last;
            last = current;
            current = temp;
        }

        previous.next = last;
        if(previous==last){
            return 1;
        }

        current = A;
        slow = last;
        while (current!=null && last!=null && current!=slow){
            if(current.val!=last.val){
                return 0;
            }
            current = current.next;
            last = last.next;
        }
        return  1;

    }
    private static void printList() {
        ListNode current = head;
        while (current!=null){
            if(current==head){
                System.out.print(current.val);
            }else{
                System.out.print(" -> "+current.val);
            }

            current=current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }
    public static void main(String[] args) {
        add(1);
        add(2);
        add(1);


//        printList();
        out(lPalin(head));

    }



}
