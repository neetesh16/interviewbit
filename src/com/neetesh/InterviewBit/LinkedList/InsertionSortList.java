package com.neetesh.InterviewBit.LinkedList;

public class InsertionSortList {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode insertionSortList(ListNode A) {
        ListNode current = A;
        ListNode start = null;
        ListNode startPtr = null;

        ListNode previous = null;
        while (current!=null){
            ListNode next = current.next;
            if(startPtr==null){
                start = current;
                startPtr = current;
            }else{
                ListNode innerPtr = start;
                previous.next = null;
                ListNode innnerPrevious = null;
                while (innerPtr!=null){
                    if(innerPtr.val>current.val){

                        break;
                    }
                    innnerPrevious = innerPtr;
                    innerPtr = innerPtr.next;
                }

                if(innnerPrevious == null){
                    start = current;

//                    current.next = start;
                }else{
                    innnerPrevious.next = current;

                }
                current.next = innerPtr;
                while (current.next!=null)current = current.next;


            }

            previous = current;
            current = next;
        }

        return start;
    }


    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{2,1,4,5,8,7,9,5};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


        printList(A);
        A = insertionSortList( A);
        printList(A);
//        printList(A);

    }


}
