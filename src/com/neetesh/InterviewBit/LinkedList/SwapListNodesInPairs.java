package com.neetesh.InterviewBit.LinkedList;

public class SwapListNodesInPairs {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" " + current.val);
            }

            current = current.next;
        }
//        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode swapPairs(ListNode A) {
//        if(B==1){printList(A);return null;}
        ListNode current = A;
        ListNode startPtr = null;
        ListNode endPtr = null;
        ListNode blockStart = null;
        ListNode blockEnd = null;
        int count = 0;
        int B = 2;
        ListNode next = null;
        while (current!=null){
            count++;
            next = current.next;
            current.next = blockStart;
            blockStart = current;
            if(count%B==0){
                if (startPtr==null){
                    startPtr = blockStart;
                    endPtr = blockEnd;
                }else{
                    endPtr.next = blockStart;
                    endPtr = blockEnd;
                }
                blockEnd = null;
                blockStart = null;
            }else{
                if(blockEnd==null)blockEnd = current;
            }
            current = next;
        }
        if(endPtr!=null)
            endPtr.next = blockStart;

        if (startPtr==null){
            startPtr = blockStart;
        }
//        printList(startPtr);
        return startPtr;
    }


    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{ 1};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


//        printList(A);
        A = swapPairs( A);
//        printList(A);
//        printList(A);

    }


}
