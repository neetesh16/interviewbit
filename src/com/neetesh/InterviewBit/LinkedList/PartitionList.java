package com.neetesh.InterviewBit.LinkedList;

import java.util.List;

public class PartitionList {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode partition(ListNode A,int B) {
        ListNode current = A;
        ListNode previous = null;
        ListNode end = A;
        int count = 0;
        while (current!=null){
            end = current;
            count++;
            current = current.next;
        }
        current = A;
        while (count>0){
            if(current.val>=B){
                ListNode next = current.next;
                current.next = null;
                if(end!=current){
                    end.next = current;
                    end = end.next;
                    if(current == A){
                        A = next;
                    }else{
                        previous.next = next;
                    }

                }else{

                }



                current = next;
            }else{
                previous = current;
                current = current.next;
            }

            count--;
        }

        return A;
    }


    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{4,5,2,3,4,5};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


        printList(A);
        A = partition( A,3);
        printList(A);
//        printList(A);

    }


}
