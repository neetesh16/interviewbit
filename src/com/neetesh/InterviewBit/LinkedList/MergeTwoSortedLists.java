package com.neetesh.InterviewBit.LinkedList;

public class MergeTwoSortedLists {

    static class ListNode {
        int val;
        ListNode  next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val,ListNode head){
        ListNode n = new ListNode(val);
        if (head == null){
            head = new ListNode(val);
        }else{
            ListNode current = head;
            while (current.next!=null)current=current.next;
            current.next = n;
        }
    }

    public static ListNode mergeTwoLists(ListNode A,ListNode B){
        ListNode start = new ListNode(-1);
        ListNode currentA = A ;
        ListNode currentB = B;
        if(currentA == null && currentB==null)return A;
        if(currentA == null && currentB!=null)return B;
        if(currentA != null && currentB==null)return A;

        start = null;
        ListNode ptr = null;
        while (currentA!=null && currentB!=null){
            if (currentA.val<currentB.val){
                if (start == null){
                    start = currentA;
                    ptr = currentA;
                }else{
                    ListNode temp = currentA;

                    start.next = temp;
                    start = start.next;
                }
                currentA = currentA.next;
            }else{
                if (start == null){
                    start = currentB;
                    ptr = currentB;
                }else{
                    ListNode temp = currentB;

                    start.next = temp;
                    start = start.next;
                }
                currentB = currentB.next;
            }

        }

        while (currentA!=null){
            start.next = currentA;
            currentA = currentA.next;
            start = start.next;
        }
        while (currentB!=null){
            start.next = currentB;
            currentB = currentB.next;
            start = start.next;
        }
        return ptr;

    }

    private static void printList(ListNode head) {
        ListNode current = head;
        while (current!=null){
            if(current==head){
                System.out.print(current.val);
            }else{
                System.out.print(" -> "+current.val);
            }

            current=current.next;
        }
        System.out.println("");
    }
    private static void out(Object o) {
        System.out.println(o);
    }
    public static void main(String[] args) {
        ListNode A = null;
        ListNode B = null;
        Integer []a = new Integer[]{5};
        Integer []b = new Integer[]{4};
        for(Integer x: a){
            if(A==null)A = new ListNode(x);
            else add(x,A);
        }
        for(Integer x: b){
            if(B==null)B = new ListNode(x);
            else add(x,B);
        }


//        printList();
        A = mergeTwoLists(A,B);
        printList(A);
//        printList(B);

    }



}
