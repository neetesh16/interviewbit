package com.neetesh.InterviewBit.LinkedList;

public class RotateList {

    static class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ListNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static void add(int val, ListNode head) {
        ListNode n = new ListNode(val);
        if (head == null) {
            head = new ListNode(val);
        } else {
            ListNode current = head;
            while (current.next != null) current = current.next;
            current.next = n;
        }
    }


    private static void printList(ListNode head) {
        ListNode current = head;
        while (current != null) {
            if (current == head) {
                System.out.print(current.val);
            } else {
                System.out.print(" -> " + current.val);
            }

            current = current.next;
        }
        System.out.println("");
    }

    private static void out(Object o) {
        System.out.println(o);
    }

    public static ListNode rotateRight(ListNode A,int B) {
        ListNode current = A;
        ListNode end = null;
        int count = 0;
        if (B==0) return A;
        while (current != null) {
            end = current;
            current = current.next;
            count++;
        }
        if(count<=0)return A;
        B = B%count;
        B = count-B+1;

        current = A;
        ListNode previous = null;
        while (B>0 && current!=null){
            B--;
            if(B==0){
                if(previous==null){

                }else{
                    previous.next = null;
                    ListNode ptr = A;
                    A = current;
                    end.next = ptr;
                }
            }else{
                previous = current;
                current = current.next;
            }
        }
        return A;
    }


    public static void main(String[] args) {
        
        ListNode A = null;
        Integer[] b = new Integer[]{91 , 34 , 18 , 83 , 38 , 82 , 21 , 69};

        for (Integer x : b) {
            if (A == null) A = new ListNode(x);
            else add(x, A);
        }


        printList(A);
        A = rotateRight( A,89);
        printList(A);
//        printList(A);

    }


}
