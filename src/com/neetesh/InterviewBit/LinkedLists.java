package com.neetesh.InterviewBit;

public class LinkedLists {
    Node head;
    class Node{
        int data;
        Node next;

        public Node(int data) {
            this.data = data;
        }
    }
    static void  out(Object l){
        System.out.println(l);
    }

    public void add(int data){
        Node n = new Node(data);
        Node end = getLastElement();
        if(end==null){
            head = n;
        }else{
            end.next = n;
            n.next = null;
        }
    }

    private Node getLastElement() {
        Node ptr = head;
        while (ptr!=null && ptr.next!=null){
             ptr = ptr.next;
        }
        return ptr;
    }

    private Node printList(Node p) {
        Node ptr = p;
        while (ptr!=null){
            System.out.print(ptr.data+" ");
            ptr = ptr.next;
        }
        System.out.println(" ");
        return ptr;
    }

    private int getCount() {
        Node ptr = head;
        int count = 0;
        while (ptr!=null){
            count++;
            ptr = ptr.next;
        }
        return count;
    }

    private int getCountRecursive(Node ptr) {
        if (ptr==null)return 0;
        else return 1+getCountRecursive(ptr.next);
    }

    private Node reverseListRecursive(Node ptr) {
        if(ptr==null)return null;
        if (ptr!=null && ptr.next==null) {
            head = ptr;
            return ptr;
        }
        Node temp = ptr;

        Node t = reverseListRecursive(ptr.next);
        temp.next = null;
        t.next = temp;
        return temp;
    }

    private Node reverseList(Node ptr) {
        Node current = ptr;
        Node previous = null;

        while (current!=null){
            Node next = current.next;
            current.next = previous;
            previous = current;
            current = next;

        }

        return previous;
    }

    private Node middlePoint(Node ptr) {
        Node current = ptr;
        Node previous = null;
        Node fast = ptr;
        Node slow = ptr;
        if (ptr==null) return null;
        while (fast!=null && fast.next!=null){
            fast = fast.next.next;
            previous = slow;
            slow = slow.next;
        }
        out("Slwo Data "+slow.data);
        previous = slow;
                slow = slow.next;

        current = slow;
        Node previous2 = null;
        while (current!=null){
            Node next = current.next;
            current.next = previous2;
            previous2 = current;
            current = next;

        }
        previous.next = previous2;
        current = head;
        printList(head);
        while (current!=null && previous2!=null && current!=previous2){
            out(current.data+"  "+previous2.data);
            current = current.next;
            previous2 = previous2.next;
        }
        return previous2;
    }


    private Node reverseHalfList(Node ptr) {
        Node current = ptr;
        Node previous = null;
        Node fast = ptr;
        Node slow = ptr;
        if (ptr==null) return null;
        while (fast!=null && fast.next!=null){
            fast = fast.next.next;
            previous = slow;
            slow = slow.next;
        }
        return slow;
    }

    public static void main(String []args){
        LinkedLists linkedLists = new LinkedLists();
        linkedLists.add(1);
        linkedLists.add(2);
        linkedLists.add(3);
        linkedLists.add(4);
        linkedLists.add(3);
        linkedLists.add(2);
        linkedLists.add(1);

//        linkedList.add(9);
        linkedLists.printList(linkedLists.head);
//        out(linkedList.getCount());
//        linkedList.reverseListRecursive(linkedList.head);


        Node middle = linkedLists.middlePoint(linkedLists.head);
//        out(middle.data);
        linkedLists.printList(linkedLists.head);
//        linkedList.printList();
    }
}
