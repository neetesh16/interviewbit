package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Permutations {
    public static ArrayList<ArrayList<Integer>> subsets(ArrayList<Integer> A) {

        int len = A.size();
        ArrayList<Integer> stack = new ArrayList<>();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        Collections.sort(A);
        subset(A,stack,len-1,0,res);
        return res;
    }

    public static void subset(List<Integer> A,ArrayList<Integer> stack,int l,int j,ArrayList<ArrayList<Integer>>res){
//        res.add(new ArrayList<>(stack));
        if(j==l){
            res.add(new ArrayList<>(A));

            return;
        }
        ArrayList<Integer> d = new ArrayList<>(A);
        for (int i=j;i<=l;i++){

            int temp = d.get(i);
            d.set(i,d.get(j));
            d.set(j,temp);

            subset(d,stack,l,j+1,res);

            temp = d.get(i);
            d.set(i,d.get(j));
            d.set(j,temp);
        }

    }
/*
[1,2,3]
[1,3,2]
[2,1,3]
[2,3,1]
[3,1,2]
[3,2,1]
 */


    public static void main(String[] args) {
        System.out.println(subsets(new ArrayList<Integer>(Arrays.asList(new Integer[]{1,2,3}))));
    }
}
