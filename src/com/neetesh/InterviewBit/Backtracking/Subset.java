package com.neetesh.InterviewBit.Backtracking;

import java.util.*;

public class Subset {
    public static ArrayList<ArrayList<Integer>> subsets(ArrayList<Integer> A) {

        int len = A.size();
        ArrayList<Integer> stack = new ArrayList<>();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        Collections.sort(A);
        subset(A,stack,len,0,res);
        return res;
    }

    public static void subset(List<Integer> A,ArrayList<Integer> stack,int l,int j,ArrayList<ArrayList<Integer>>res){
        res.add(new ArrayList<>(stack));
        if(j==l)return;
        for (int i=j;i<l;i++){
            stack.add(A.get(i));
            subset(A,stack,l,i+1,res);
            stack.remove(stack.size() - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(subsets(new ArrayList<Integer>(Arrays.asList(new Integer[]{1,2,3}))));
    }
}
