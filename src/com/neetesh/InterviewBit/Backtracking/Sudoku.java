package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;

public class Sudoku {


    static void out(Object l) {
        System.out.println(l);
    }

    public static void solveSudoku(ArrayList<ArrayList<Character>> A) {
        int len = A.size();
        findSolution(A);
        return;
    }

    public static boolean findSolution(ArrayList<ArrayList<Character>> A) {
        if (checkSolution(A)) {
            return true;
        }
        int flag = 0;
        int i = 0;
        int j = 0;
        for (i = 0; i < 9; i++) {
            for (j = 0; j < 9; j++) {
                if (A.get(i).get(j) == '.') {
                    flag = 1;
                    break;
                }

            }
            if (flag == 1) break;

        }
        if (i >= 9 || j >= 9) {
            if (checkSolution(A)) {
                return true;
            }
        }

        if (A.get(i).get(j) == '.') {
            for (int k = 1; k <= 9; k++) {
                if (checkUse(A, i, j, k)) {
                    A.get(i).set(j,(char) (k + '0'));
                    boolean soln = findSolution(A);
                    if (soln == true) {
                        return true;
                    }
                    A.get(i).set(j,'.');
                }

            }
        }
        return false;

    }

    public static boolean checkUse(ArrayList<ArrayList<Character>> A, int i, int j, int num) {
        return (!usedInSquare(A, i - i % 3, j - j % 3, num) && usedInRow(A, i, j, num) && usedInColumn(A, i, j, num));
    }

    public static boolean usedInSquare(ArrayList<ArrayList<Character>> A, int col, int row, int num) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int p = (int) (A.get(col + i).get(row + j) - '0');
                if (p == num) {
                    return true;
                }

            }

        }
        return false;
    }

    public static boolean usedInRow(ArrayList<ArrayList<Character>> A, int col, int row, int num) {
        int column[] = new int[10];
        for (int i = 0; i < 9; i++) {
            if (A.get(col).get(i) == '.') continue;
            int t = (int) (A.get(col).get(i) - '0');
            column[t] = column[t] + 1;
        }

        return (column[num] == 0);
    }

    public static boolean usedInColumn(ArrayList<ArrayList<Character>> A, int col, int row, int num) {
        int rows[] = new int[10];
        for (int i = 1; i < 10; i++) {
            rows[i] = 0;
        }

        for (ArrayList<Character> s : A) {
            if (s.get(row) == '.') continue;
            int t = (int) (s.get(row) - '0');
            rows[t] = rows[t] + 1;
        }
        return (rows[num] == 0);
    }

    public static boolean checkSolution(ArrayList<ArrayList<Character> > A) {
        int rows[] = new int[10];
        int column[] = new int[10];
        int box[] = new int[10];
        for (int i = 1; i < 10; i++) {
            rows[i] = 0;
            column[i] = 0;
        }

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (A.get(i).get(j) == '.') return false;
            }
        }
        for (int i = 0; i < 9; i++) {
            for (ArrayList<Character> s : A) {
                if (s.get(i) == '.') return false;
                int t = (int) (s.get(i) - '0');
                rows[t] = rows[t] + 1;
            }
            for (int k = 1; k <= 9; k++) {
                if (rows[k] != 1) {
                    return false;
                }
                rows[k] = 0;
            }

        }


        for (ArrayList<Character> s : A) {
            for (int i = 0; i < 9; i++) {
                if (s.get(i) == '.') return false;
                int t = (int) (s.get(i) - '0');
                column[t] = column[t] + 1;
            }
            for (int k = 1; k <= 9; k++) {
                if (column[k] != 1) {
                    return false;
                }
                column[k] = 0;
            }
        }

        for (int row = 0; row < 9; row += 3) {
            for (int col = 0; col < 9; col += 3) {
                for (int i = 0; i < 3; i++) {
                    for (int j = 0; j < 3; j++) {
                        int p = (int) (A.get(col + i).get(row + j) - '0');
                        rows[p] = rows[p] + 1;

                    }

                }

                for (int k = 1; k <= 9; k++) {
                    if (rows[k] != 1) {
                        return false;
                    }
                    rows[k] = 0;
                }

            }

        }

        return true;

    }


    public static void main(String[] args) {

//        String[] o = new String[]{"534678912", "672195348", "198342567", "859761423", "426853791", "713924856", "961537284", "287419635", "345286179"};
        String[] o = new String[]{"53..7....", "6..195...", ".98....6.", "8...6...3", "4..8.3..1", "7...2...6", ".6....28.", "...419..5", "....8..79"};
        ArrayList<ArrayList<Character>> p = new ArrayList<>();
        for (String s:o){
            ArrayList<Character> a=new ArrayList<Character>();
            for(int i=0;i<s.length();i++)
            {
                a.add(s.charAt(i));

            }
            p.add(new ArrayList<>(a));
        }
//        out(p);
        solveSudoku(p);
        out(p);
//        for (String s : p) {
//            out(s);
//        }
//        System.out.println(letterCombinations("2323"));
    }
}
