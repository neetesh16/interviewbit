package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Combinations {
    public static ArrayList<ArrayList<Integer>> combine(int A, int B) {

        ArrayList<Integer> stack = new ArrayList<>();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        kcombinations(A,stack,A,1,res,B);
        return res;
    }

    public static void kcombinations(int A,ArrayList<Integer> stack,int l,int j,ArrayList<ArrayList<Integer>>res,int B){

        if(stack.size()==B){
            res.add(new ArrayList<>(stack));
            return;
        }
        for (int i=j;i<=l;i++){
            stack.add(i);
            kcombinations(A,stack,l,i+1,res,B);
            stack.remove(stack.size() - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(combine(4,3));
    }
}
