package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class KthPermutationSequence {
    public static String getPermutation(int n,int k) {
        ArrayList <Integer> a = new ArrayList<>();
        for (int i=0;i<=n;i++){
            a.add(i);
        }
        String res = findAnswer(n,k,a,"");
        return res;
    }

    public static int factorial(int number){
        if (number > 12) {
            // this overflows in int. So, its definitely greater than k
            // which is all we care about. So, we return INT_MAX which
            // is also greater than k.
            return Integer.MAX_VALUE;
        }
        int i,fact=1;

        for(i=1;i<=number;i++){
            fact=fact*i;
        }
        return fact;
    }

    public static String findAnswer(int n,int k,ArrayList <Integer> a, String ans){

        if(n==0)return ans;
        int temp = factorial(n-1);
        int item = (int)Math.ceil((k+0.0)/temp);
        int j = item;
        if(item>=a.size())return "";
        item  =  a.get(item);
        a.remove(a.indexOf(item));
        ans+=item+"";
        k = k%temp;
        if(k==0)
        k = temp;
        ans = findAnswer(n-1,k, a,ans);
        return ans;
    }
/*
[1,2,3]
[1,3,2]
[2,1,3]
[2,3,1]
[3,1,2]
[3,2,1]
 */


    public static void main(String[] args) {
        System.out.println(getPermutation(3,4));
    }
}
