package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SubsetII {
    public static ArrayList<ArrayList<Integer>> subsetsWithDup(ArrayList<Integer> A) {

        int len = A.size();
        ArrayList<Integer> stack = new ArrayList<>();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        Collections.sort(A);
        subset(A,stack,len,0,res);
        return res;
    }

    public static void subset(List<Integer> A,ArrayList<Integer> stack,int l,int j,ArrayList<ArrayList<Integer>>res){
        res.add(new ArrayList<>(stack));
        if(j==l)return;
        for (int i=j;i<l;i++){
            if(i!=j && A.get(i)==A.get(i-1)){
                continue;
            }
            stack.add(A.get(i));
            subset(A,stack,l,i+1,res);
            stack.remove(stack.size() - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(subsetsWithDup(new ArrayList<Integer>(Arrays.asList(new Integer[]{1,2,2}))));
    }
}
