package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GenerateAllParenthesesIi {
    public static ArrayList<String> generateParenthesis(int A) {

        int len = A;
        String stack = "";
        ArrayList<String> res = new ArrayList<>();

        subset(0, stack, 0, res, len);
        return res;
    }

    public static void subset(int A, String stack, int l, ArrayList<String> res, int p) {

        if (l==p && A==p) {
            res.add(stack);
            return;
        }else{

            String temp = stack;
            if(A<p){
                temp += "(";
                subset(A+1 , temp, l, res, p);
            }
            temp = stack;
            if(A>l){
                temp += ")";
                subset(A , temp, l+1, res, p);

            }
        }
    }


    public static void main(String[] args) {
        System.out.println(generateParenthesis(3));
    }
}
