package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;

public class PalindromePartitioning {
    static ArrayList<ArrayList<String>>p = new ArrayList<>();


    public static ArrayList<ArrayList<String>> partition(String A) {
        int len = A.length();
        ArrayList<String> stack = new ArrayList<>();
        ArrayList<ArrayList<String>> res = new ArrayList<>();
        subset(A,stack,len,0,res);
        return res;
    }

    public static boolean isPalindrome(String s){
        StringBuilder b = new StringBuilder(s);
        if(s.equals(b.reverse().toString())){
            return  true;
        }
        return false;
    }
    public static void subset(String A,ArrayList<String> stack,int l,int start,ArrayList<ArrayList<String>>res){

        if(start>=l){
            if(!stack.isEmpty())
                res.add(new ArrayList<>(stack));
            return;
        }
        int len = 1;
        while (start+len<=l){
            String temp = A.substring(start,start+len);
            if(isPalindrome(temp)){
                stack.add(temp);
                subset(A,stack,l,start+len,res);
                stack.remove(stack.size() - 1);
            }
            len++;
        }
    }
    public static void main(String[] args) {
        System.out.println(partition("aab"));
        System.out.println(partition("nitin"));
    }
}
