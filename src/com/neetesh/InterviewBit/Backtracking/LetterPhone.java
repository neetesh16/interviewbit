package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class LetterPhone {
    static ArrayList<ArrayList<String>>p = new ArrayList<>();

    public static void fillMap(){
        p.clear();
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"0"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"1"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"a","b","c"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"d","e","f"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"g","h","i"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"j","k","l"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"m","n","o"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"p","q","r","s"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"t","u","v"})));
        p.add(new ArrayList<String>(Arrays.asList(new String[]{"w","x","y","z"})));
    }

    public static ArrayList<String> letterCombinations(String A) {
        fillMap();
        int len = A.length();
        String stack = "";
        ArrayList<String> res = new ArrayList<>();
        subset(A,stack,len,0,res,0);
        return res;
    }

    public static void subset(String A,String stack,int l,int i,ArrayList<String>res,int j){

        if(j==l){
            res.add((stack));
            return;
        }
        Integer num = Integer.parseInt(A.charAt(j)+"");
        for (i=0;i<p.get(num).size();i++){
            stack+=(p.get(num).get(i));
            subset(A,stack,l,i,res,j+1);
            stack = stack.substring(0,stack.length() - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(letterCombinations("0"));
//        System.out.println(letterCombinations("2323"));
    }
}
