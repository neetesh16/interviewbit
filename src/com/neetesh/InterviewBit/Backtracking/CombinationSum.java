package com.neetesh.InterviewBit.Backtracking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CombinationSum {
    public static ArrayList<ArrayList<Integer>> combinationSum(ArrayList<Integer> A,int B) {

        int len = A.size();
        ArrayList<Integer> stack = new ArrayList<>();
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        Collections.sort(A);
        subset(A,stack,len,0,res,B);
        return res;
    }

    public static void subset(List<Integer> A, ArrayList<Integer> stack, int l, int j, ArrayList<ArrayList<Integer>>res,int B){

        if(B==0){
            res.add(new ArrayList<>(stack));
            return;
        }
        if (B<0)return;
        for (int i=j;i<l;i++){
            if(i>0 && A.get(i).equals(A.get(i-1))){
                continue;
            }
            stack.add(A.get(i));
            subset(A,stack,l,i,res,B-A.get(i));
            stack.remove(stack.size() - 1);
        }
    }

    public static void main(String[] args) {
//        System.out.println(combinationSum(new ArrayList<Integer>(Arrays.asList(new Integer[]{2, 3, 6, 7})),7));
        System.out.println(combinationSum(new ArrayList<Integer>(Arrays.asList(new Integer[]{8, 10, 6, 11, 1, 16, 8})),28));
    }
}
