package com.neetesh.InterviewBit.Trees;

import sun.reflect.generics.tree.Tree;

import java.util.*;

public class ZigZagLevelOrderTraversalBt {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Pair{
        TreeNode node;
        int distance ;

        public Pair(TreeNode node, int distance) {
            this.node = node;
            this.distance = distance;
        }
    }

    public static ArrayList<ArrayList<Integer> > zigzagLevelOrder(TreeNode a) {
        ArrayList<ArrayList<Integer> > result = new ArrayList<>();
        Deque<TreeNode>q = new LinkedList<>();
        if(a==null) return result;
        q.addFirst(a);
        int c = 1;
        int flag = 0;
        while (!q.isEmpty()){
            ArrayList<Integer> levels = new ArrayList<>();
            int temp = c;
            c = 0;
            while (temp>0){
                TreeNode curr ;
                if(flag==0){
                    curr = q.removeFirst();
                    levels.add(curr.val);
                    if(curr.left!=null){
                        c++;
                        q.addLast(curr.left);
                    }
                    if(curr.right!=null){
                        c++;
                        q.addLast(curr.right);
                    }
                }else{
                    curr = q.removeLast();
                    levels.add(curr.val);
                    if(curr.right!=null){
                        c++;
                        q.addFirst(curr.right);
                    }
                    if(curr.left!=null){
                        c++;
                        q.addFirst(curr.left);
                    }

                }
                temp--;
            }
            flag = 1-flag;
            result.add(levels);

        }

        return result;
    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode(5);
        A.left = new TreeNode(4);
        A.right = new TreeNode(8);
        A.left.left = new TreeNode(1);
        A.left.left.right = new TreeNode(2);
        A.left.left.left = new TreeNode(7);
        A.right.right = new TreeNode(4);
        A.right.right.right = new TreeNode(1);
        A.right.right.left = new TreeNode(5);
        A.right.left = new TreeNode(3);
        out(zigzagLevelOrder(A));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//            5
//          /  \
//         4    8
//        /    / \
//        1   3  4
//       / \    / \
//     7    2  5   1



