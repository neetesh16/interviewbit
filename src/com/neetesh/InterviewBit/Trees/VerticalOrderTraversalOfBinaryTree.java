package com.neetesh.InterviewBit.Trees;

import sun.reflect.generics.tree.Tree;

import java.util.*;

public class VerticalOrderTraversalOfBinaryTree {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Pair{
        TreeNode node;
        int distance ;

        public Pair(TreeNode node, int distance) {
            this.node = node;
            this.distance = distance;
        }
    }

    public static ArrayList<ArrayList<Integer> > verticalOrderTraversal(TreeNode a) {
        ArrayList<ArrayList<Integer> > result = new ArrayList<>();
        Queue<Pair> q = new LinkedList<>();
        TreeMap<Integer,ArrayList<Integer>>map = new TreeMap<>();
        q.add(new Pair(a,0));
        ArrayList<Integer> temp = new ArrayList<>();
        while (!q.isEmpty()){
            Pair p = q.peek();
            q.remove();
            if(map.containsKey(p.distance)){
                temp = map.get(p.distance);
                temp.add(p.node.val);
                map.put(p.distance,temp);
            }else{
                ArrayList<Integer> pw = new ArrayList<>();
                pw.add(p.node.val);
                map.put(p.distance,pw);
            }
            if (p.node.left!=null){
                q.add(new Pair(p.node.left,p.distance - 1));
            }
            if(p.node.right!=null){
                q.add(new Pair(p.node.right,p.distance + 1));
            }

        }

        for(Map.Entry<Integer,ArrayList<Integer> >it:map.entrySet()){
            result.add(it.getValue());
        }
        return result;
    }

    public static void main(String[] args) {
        TreeNode a = new TreeNode(6);
            a.left = new TreeNode(3);
        a.left.left = new TreeNode(2);
        a.left.right = new TreeNode(5);
        a.right = new TreeNode(7);
        a.right.right = new TreeNode(9);
        out(verticalOrderTraversal(a));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        1
//    2      3
//  4   5  6



