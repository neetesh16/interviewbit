package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class ConstructBinaryTreeFromInorderAndPreorder {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static ArrayList<Integer> preorderTraversal(TreeNode a) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(a == null)
            return result;
        TreeNode node = a;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                result.add(node.val);
                node = node.left;
            }
            else{
                node = stack.pop();
                node = node.right;
            }
        }
        return result;
    }
    
    public static ArrayList<Integer> inorderTraversal(TreeNode a) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(a == null)
            return result;
        TreeNode node = a;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                node = node.left;
            }
            else{
                node = stack.pop();
                result.add(node.val);
                node = node.right;
            }
        }
        return result;
    }
    public static void main(String[] args) {
        Integer[] b1 = new Integer[]{1, 2, 3, 4, 5 };
        Integer[] b2 = new Integer[]{3, 2, 4, 1, 5};
        ArrayList<Integer> inOrder = new ArrayList(Arrays.asList(b1));
        ArrayList<Integer> postOrder = new ArrayList(Arrays.asList(b2));
        ArrayList<Integer> v = inorderTraversal(buildTree(inOrder,postOrder));
        out(v);
        v = preorderTraversal(buildTree(inOrder,postOrder));
        out(v);
    }

    static class Node{

        int ptr ;

        public Node(int ptr) {
            this.ptr = ptr;
        }
    }

    public static TreeNode buildTree(ArrayList<Integer> postOrder,ArrayList<Integer> inOrder) {
        int currPtr = 0;
        Node ptr = new Node(currPtr);
        TreeNode root = buildTreeUtil(inOrder,postOrder,ptr,0,inOrder.size()-1);
        return root;
    }

    public static TreeNode buildTreeUtil(ArrayList<Integer> inOrder,ArrayList<Integer> postOrder,Node currPtr,int start,int end) {
        if(start>end)return null;
        if(currPtr.ptr>inOrder.size()-1)return null;

        TreeNode n = new TreeNode(postOrder.get(currPtr.ptr));
        int pos = findInItem(inOrder,postOrder.get(currPtr.ptr),start,end);

        currPtr.ptr++;

        if(start==end){
            return n;
        }
        TreeNode left=buildTreeUtil(inOrder,postOrder,currPtr,start,pos-1);
        TreeNode right = buildTreeUtil(inOrder,postOrder,currPtr,pos+1,end);

        n.left = left;
        n.right = right;
        return n;
    }

    public static int findInItem(ArrayList<Integer> a,int ele,int start,int end) {

        for (int i=start;i<=end;i++){
            if(a.get(i).equals(ele)){
                return i;
            }
        }
        return 0;
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
