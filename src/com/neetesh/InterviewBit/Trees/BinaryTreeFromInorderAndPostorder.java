package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class BinaryTreeFromInorderAndPostorder {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }
    public static ArrayList<Integer> inorderTraversal(TreeNode a) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(a == null)
            return result;
        TreeNode node = a;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                node = node.left;
            }
            else{
                node = stack.pop();
                result.add(node.val);
                node = node.right;
            }
        }
        return result;
    }
    public static void main(String[] args) {
        Integer[] b1 = new Integer[]{4, 8, 2, 5, 1, 6, 3, 7};
        Integer[] b2 = new Integer[]{8, 4, 5, 2, 6, 7, 3, 1};
        ArrayList<Integer> inOrder = new ArrayList(Arrays.asList(b1));
        ArrayList<Integer> postOrder = new ArrayList(Arrays.asList(b2));
        ArrayList<Integer> v = inorderTraversal(buildTree(inOrder,postOrder));
        out(v);
    }

    static class Node{

        int ptr ;

        public Node(int ptr) {
            this.ptr = ptr;
        }
    }

    public static TreeNode buildTree(ArrayList<Integer> inOrder,ArrayList<Integer> postOrder) {
        int currPtr = postOrder.size() - 1;
        Node ptr = new Node(currPtr);
        TreeNode root = buildTreeUtil(inOrder,postOrder,ptr,0,currPtr);
        return root;
    }

    public static TreeNode buildTreeUtil(ArrayList<Integer> inOrder,ArrayList<Integer> postOrder,Node currPtr,int start,int end) {
        if(start>end)return null;
        int pos = findInItem(inOrder,postOrder.get(currPtr.ptr),start,end);
        TreeNode n = new TreeNode(inOrder.get(pos));
        currPtr.ptr--;
        TreeNode right = buildTreeUtil(inOrder,postOrder,currPtr,pos+1,end);
        TreeNode left=buildTreeUtil(inOrder,postOrder,currPtr,start,pos-1);
        n.left = left;
        n.right = right;
        return n;
    }

    public static int findInItem(ArrayList<Integer> a,int ele,int start,int end) {

        for (int i=start;i<=end;i++){
            if(a.get(i).equals(ele)){
                return i;
            }
        }
        return 0;
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
