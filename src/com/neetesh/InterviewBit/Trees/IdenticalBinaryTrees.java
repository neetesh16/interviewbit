package com.neetesh.InterviewBit.Trees;

public class IdenticalBinaryTrees {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{

        int height ;

        public Node(int distance) {
            this.height = distance;
        }
    }

    public static int isSameTree(TreeNode A, TreeNode B) {
        if(A==null && B==null) return 1;
        if(A==null || B==null) return 0;
        if (A.val==B.val && isSameTree(A.left,B.left)==1 && isSameTree(A.right,B.right)==1){
           return 1;
        }
        return 0;
    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode(6);
            A.left = new TreeNode(3);
        A.left.left = new TreeNode(2);
        A.left.right = new TreeNode(5);
        A.right = new TreeNode(7);
//        A.right.right = new TreeNode(9);

        TreeNode B = new TreeNode(6);
        B.left = new TreeNode(3);
        B.left.left = new TreeNode(2);
        B.left.right = new TreeNode(5);
        B.right = new TreeNode(7);
        out(isSameTree(A,B));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        6
//    3      7
//  2   5       9


