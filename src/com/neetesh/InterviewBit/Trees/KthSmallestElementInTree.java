package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Stack;

public class KthSmallestElementInTree {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    public static ArrayList<Integer> inorderTraversal(TreeNode A) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(A == null)
            return result;
        TreeNode node = A;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                node = node.left;
            }
            else{
                node = stack.pop();
                result.add(node.val);
                node = node.right;
            }
        }
        return result;
    }

    public static int kthElement(TreeNode A,int B){
        TreeNode curr = A;
        Stack<TreeNode>s = new Stack<>();

        while (!s.isEmpty() || curr!=null){
            if(curr!=null){
                s.push(curr);
                curr = curr.left;
            }else{
                curr = s.pop();
                B--;
                if(B==0)return curr.val;
                curr = curr.right;
            }
        }


        return 0;
    }
    public static void main(String[] args) {
        TreeNode A = new TreeNode(1);
        A.left = new TreeNode(2);
        A.left.left = new TreeNode(4);
        A.left.right = new TreeNode(5);
        A.right = new TreeNode(3);
        A.right.left = new TreeNode(6);
        out(kthElement(A,4));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        1
//    2       3
// 4     5   6