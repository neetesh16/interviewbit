package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class SortedArrayToBalancedBST {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }
    public static ArrayList<Integer> inorderTraversal(TreeNode a) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(a == null)
            return result;
        TreeNode node = a;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                node = node.left;
            }
            else{
                node = stack.pop();
                result.add(node.val);
                node = node.right;
            }
        }
        return result;
    }
    public static void main(String[] args) {
        Integer[] b = new Integer[]{1,2,3,4,5,6};
        ArrayList<Integer> a = new ArrayList(Arrays.asList(b));
        ArrayList<Integer> v = inorderTraversal(sortedArrayToBST(a));
        out(v);
    }

    private static TreeNode sortedArrayToBST(final List<Integer> a) {
        int l = 0;int r = a.size()-1;
        TreeNode root = sortedArrayToBSTUtil(l,r,a);
        return root;
    }

    private static TreeNode sortedArrayToBSTUtil(int l , int r,List<Integer> a) {
        int mid = (int)Math.ceil((r+l)/2.0);
        if(l>r){
            return null;
        }
        if(l==r){
            return  new TreeNode(a.get(l));
        }
        TreeNode node = new TreeNode(a.get(mid));
        TreeNode left=sortedArrayToBSTUtil(l,mid-1,a);
        TreeNode right = sortedArrayToBSTUtil(mid+1,r,a);
        node.left = left;
        node.right = right;
        return node;
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
