package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Stack;

public class PreorderTraversal {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }


    public static ArrayList<Integer> preorderTraversal(TreeNode a) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(a == null)
            return result;
        TreeNode node = a;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                result.add(node.val);
                node = node.left;
            }
            else{
                node = stack.pop();
                node = node.right;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        TreeNode a = new TreeNode(1);
        a.left = new TreeNode(2);
        a.left.left = new TreeNode(4);
        a.left.right = new TreeNode(5);
        a.right = new TreeNode(3);
        a.right.left = new TreeNode(6);
        out(preorderTraversal(a));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
