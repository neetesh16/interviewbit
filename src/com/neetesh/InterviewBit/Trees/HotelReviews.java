package com.neetesh.InterviewBit.Trees;

import sun.reflect.generics.tree.Tree;

import java.util.*;

public class HotelReviews {

    static class TreeNode {
        boolean endNode;
        ArrayList<TreeNode> nodes= new ArrayList<>();

        public TreeNode() {
            this.endNode = false;
            for (int i=0;i<27;i++){
                nodes.add(null);
            }
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            for (int i=0;i<27;i++){
                if(nodes.get(i)!=null){
                    sb.append((char)(i+'a'));
                    sb.append(',');
                }
            }
            sb.append('}');
            return sb.toString();
        }

    }

    public  static TreeNode insert(TreeNode a,String s ){
        TreeNode curr = a;

        for (int i=0;i<s.length();i++){
            int c  =(int) s.charAt(i)-'a';
            if(a.nodes.get(c)==null){
               a.nodes.set(c,new TreeNode());

            }
            a = a.nodes.get(c);

        }
        a.endNode = true;
        return curr;
    }

    public  static boolean checkWord(TreeNode a,String s ){
        TreeNode curr = a;

        for (int i=0;i<s.length();i++){
            int c  =(int) s.charAt(i)-'a';
            if(curr.nodes.get(c)==null){
                return false;
            }
            curr = curr.nodes.get(c);

        }
        return curr.endNode;
    }

    static class Data{
        int id;
        int count;

        public Data(int id, int count) {
            this.id = id;
            this.count = count;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Data{");
            sb.append(id);
            sb.append(" , "+count);
            sb.append('}');
            return sb.toString();
        }

    }
    public  static ArrayList<Integer> solve(String s, ArrayList<String>reviews){
        ArrayList<Data>data = new ArrayList<>();
        TreeNode A = new TreeNode();
        ArrayList<String> tokens = new ArrayList<>(Arrays.asList(s.split("_")));
        int tsize = tokens.size();
        for (int i=0;i<tsize;i++){
            A = insert(A,tokens.get(i));
        }

        int rlen = reviews.size();
        for (int i=0;i<rlen;i++){
            ArrayList<String> review = new ArrayList<>(Arrays.asList(reviews.get(i).split("_")));
            int c = 0;
            int rl = review.size();
            for (int j=0;j<rl;j++){
                if(checkWord(A,review.get(j))){
                    c++;
                }
            }
            data.add(new Data(i,c));
//            out(c+" "+reviews.get(i));
        }

        Collections.sort(data,new Comparator<Data>(){
            @Override
            public int compare(Data d1,Data d2){
                if(d2.count==d1.count){
                    return d1.id-d2.id;
                }
                return d2.count-d1.count;
            }
        });
        ArrayList<Integer>res = new ArrayList<>();
        for (int i=0;i<rlen;i++){
            res.add(data.get(i).id);
        }

        return res;

    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode();
        String []rev = new String[]{"water_is_cool", "cold_ice_drink", "cool_wifi_speed"};
        ArrayList<String> review = new ArrayList<String>(Arrays.asList(rev));
        out(solve("cool_ice_wifi",review));

    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        1
//    2       3
// 4     5   6