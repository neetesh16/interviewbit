package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;

public class PathSum {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{
        int sum;

        public Node(int d1) {
           this.sum = d1;
        }
    }

    public static ArrayList<ArrayList<Integer>> pathSum(TreeNode A, int B) {
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        ArrayList<Integer>path = new ArrayList<>();
        hasPathSum(A,B,path,res);
        return res;
    }

    public static int hasPathSum(TreeNode A, int B, ArrayList<Integer>path,ArrayList<ArrayList<Integer>> res) {
        path.add(A.val);
        if (A==null && B==0) {
            res.add(new ArrayList<>(path));
            path.remove(path.size()-1);
            return 1;
        }

        if(B-A.val==0 && A.left==null && A.right==null){
            res.add(new ArrayList<>(path));
            path.remove(path.size()-1);
            return 1;
        }

        if(A.left!=null){
            hasPathSum(A.left,B-A.val,path,res);
        }
        if(A.right!=null){
            hasPathSum(A.right,B-A.val,path,res);
        }
        path.remove(path.size()-1);


        return 0;
    }

    public static void main(String[] args) {
//        TreeNode A = new TreeNode(5);
//        A.left = new TreeNode(4);
//        A.right = new TreeNode(8);
//        A.left.left = new TreeNode(11);
//        A.left.left.right = new TreeNode(2);
//        A.left.left.left = new TreeNode(7);
//        A.right.right = new TreeNode(4);
//        A.right.right.right = new TreeNode(1);
//        A.right.right.left = new TreeNode(5);
//        A.right.left = new TreeNode(13);

        TreeNode A = new TreeNode(1000);
        A.left = new TreeNode(2000);
        A.left.left = new TreeNode(-3001);

        Node B = new Node(22);
        ArrayList<Integer>path = new ArrayList<>();
        out(pathSum(A,-1));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        6
//    3      7
//  2   5   5    9


