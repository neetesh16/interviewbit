package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Stack;

public class LeastCommonAncestor {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{

        int d1 ;
        int d2 ;
        boolean fd1 = false ;
        boolean fd2  = false;
        boolean firstTime  = false;

        public Node(int d1, int d2) {
            this.d1 = d1;
            this.d2 = d2;
        }
    }

    public static TreeNode lcaUtil(TreeNode A,Node N) {
        if (A==null) return null;

        if (N.fd1==false && A.val == N.d1){
            N.fd1 = true;
            return A;
        }

        if (N.fd2==false && A.val == N.d2){
            N.fd2 = true;
            return A;
        }


        TreeNode leftSubStree = lcaUtil(A.left,N);
        TreeNode rightSubStree = lcaUtil(A.right,N);

        if(leftSubStree!=null && rightSubStree!=null){
            return A;
        }
        if(leftSubStree!=null) return leftSubStree;
        else if(rightSubStree!=null) return rightSubStree;
        return null;
    }

    public static int lca(TreeNode A,int B,int C) {
        if (A == null) return -1;
        Node n = new Node(B, C);
        TreeNode X;
//        X = lcaUtil(A, n);
        if (n.d1 == A.val) {
            n.fd1 = true;
            X = lcaUtil(A, n);
            X = A;
        } else if (n.d2 == A.val) {
            n.fd2 = true;
            X = lcaUtil(A, n);
            X = A;
        } else {
            X = lcaUtil(A, n);
            if(X!=null){
                if (n.d1 == X.val) {
                    n.fd1 = true;
                    lcaUtil(A, n);
                } else if (n.d2 == X.val) {
                    n.fd2 = true;
                    lcaUtil(A, n);

                }
            }

        }

        if (n.fd1 && n.fd2) {
            return X.val;
        } else {
            return -1;
        }
    }


    public static void main(String[] args) {
        TreeNode A = new TreeNode(3);
        A.left = new TreeNode(5);
        A.right = new TreeNode(1);
        A.left.left = new TreeNode(6);
        A.left.right = new TreeNode(2);
        A.left.right.left = new TreeNode(7);
        A.left.right.right = new TreeNode(4);

        A.right.right = new TreeNode(8);
        A.right.left = new TreeNode(0);

        out(lca(A,3,4));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        6
//    3      7
//  2   5   5    9


