package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;

public class PopulateNextRightPointersTree {
    static class TreeLinkNode {
        int val;
        TreeLinkNode left;
        TreeLinkNode right;
        TreeLinkNode next;

        public TreeLinkNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
            this.next = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeLinkNode{");
            sb.append("x=").append(val);
            sb.append(" , next=").append(next);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Pair{
        TreeLinkNode node;
        int distance ;

        public Pair(TreeLinkNode node, int distance) {
            this.node = node;
            this.distance = distance;
        }
    }
//            5
//          /  \
//         4 -> 8
//        /    / \
//        1   3->4
//       / \    / \
//     7    2  5 ->1
    // Right Node Update

    
    public static void connect(TreeLinkNode A) {
        if(A==null) return;
        TreeLinkNode curr = A.next;
        if(A.right!=null){
                while (curr!=null){
                    if(curr.left!=null){
                        A.right.next = curr.left;
                        break;
                    }else if(curr.right!=null){
                        A.right.next = curr.right;
                        break;
                    }
                    curr = curr.next;
                }
        }
        //Left Right Update
        if(A.left!=null){
            if(A.right!=null){
                A.left.next = A.right;
            }else{
                curr = A.next;
                while (curr!=null){
                    if(curr.left!=null){
                        A.left.next = curr.left;
                        break;
                    }else if(curr.right!=null){
                        A.left.next = curr.right;
                        break;
                    }
                    curr = curr.next;
                }
            }
        }
        connect(A.right);
        connect(A.left);
    }

    public static void main(String[] args) {
        TreeLinkNode A = new TreeLinkNode(5);
        A.left = new TreeLinkNode(4);
        A.right = new TreeLinkNode(8);
        A.left.left = new TreeLinkNode(1);
//        A.left.left.right = new TreeLinkNode(2);
        A.left.left.left = new TreeLinkNode(7);
        A.right.right = new TreeLinkNode(4);
        A.right.right.right = new TreeLinkNode(1);
        A.right.right.left = new TreeLinkNode(5);
        A.right.left = new TreeLinkNode(3);
        connect(A);
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//            5
//          /  \
//         4    8
//        /    / \
//        1   3  4
//       / \    / \
//     7    2  5   1



