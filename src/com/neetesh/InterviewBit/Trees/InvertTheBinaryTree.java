package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;
import java.util.Stack;

public class InvertTheBinaryTree {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{

        int height ;

        public Node(int distance) {
            this.height = distance;
        }
    }

    public static int isSymmetricUtil(TreeNode A, TreeNode B) {
        if(A==null && B==null) return 1;
        if(A==null || B==null) return 0;
        if (A.val==B.val && isSymmetricUtil(A.left,B.right)==1 && isSymmetricUtil(A.right,B.left)==1){
           return 1;
        }
        return 0;
    }

    public static TreeNode invertTree(TreeNode A) {
        if(A==null) return null;

        invertTree(A.left);
        invertTree(A.right);
        TreeNode temp = A.left;
        A.left = A.right;
        A.right = temp;
        return A;
    }

    public static ArrayList<Integer> inorderTraversal(TreeNode a) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        Stack<TreeNode> stack = new Stack<TreeNode>();
        if(a == null)
            return result;
        TreeNode node = a;
        while(!stack.isEmpty() || node != null){
            if(node != null){
                stack.push(node);
                node = node.left;
            }
            else{
                node = stack.pop();
                result.add(node.val);
                node = node.right;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode(6);
            A.left = new TreeNode(18);
        A.left.left = new TreeNode(15);
        A.left.right = new TreeNode(14);
        A.right = new TreeNode(12);
        A.right.right = new TreeNode(11);
        A.right.left = new TreeNode(10);
        out(inorderTraversal(A));
        out(invertTree(A));
        out(inorderTraversal(invertTree(A)));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        6
//    3      7
//  2   5   5    9


