package com.neetesh.InterviewBit.Trees;

public class MinDepthofBinaryTree {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{
        int sum;

        public Node(int d1) {
           this.sum = d1;
        }
    }

    public static int minDepth(TreeNode A) {
        return minDepthUtil(A,0);
    }

    public static int minDepthUtil(TreeNode A, int lvl) {
        if(A==null){
            return Integer.MAX_VALUE;
        }
        if(A.left == null && A.right==null){
            return (lvl+1);
        }
        int l = minDepthUtil(A.left,lvl+1);
        int r = minDepthUtil(A.right,lvl+1);
        return Math.min(l,r);
    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode(5);
        A.left = new TreeNode(4);
        A.right = new TreeNode(8);
//        A.left.left = new TreeNode(11);
//        A.left.left.right = new TreeNode(2);
//        A.left.left.left = new TreeNode(7);
//        A.right.right = new TreeNode(4);
//        A.right.right.right = new TreeNode(1);
//        A.right.right.left = new TreeNode(5);
//        A.right.left = new TreeNode(13);

//        TreeNode A = new TreeNode(1000);
//        A.left = new TreeNode(2000);
//        A.left.left = new TreeNode(-3001);

        out(minDepth(A));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//            5
//          /  \
//         4    8
//        /    / \
//        1   3  4
//       / \    / \
//     7    2  5   1

