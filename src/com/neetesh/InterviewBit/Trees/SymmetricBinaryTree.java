package com.neetesh.InterviewBit.Trees;

public class SymmetricBinaryTree {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{

        int height ;

        public Node(int distance) {
            this.height = distance;
        }
    }

    public static int isSymmetricUtil(TreeNode A, TreeNode B) {
        if(A==null && B==null) return 1;
        if(A==null || B==null) return 0;
        if (A.val==B.val && isSymmetricUtil(A.left,B.right)==1 && isSymmetricUtil(A.right,B.left)==1){
           return 1;
        }
        return 0;
    }

    public static int isSymmetric(TreeNode A) {
        if(A==null ) return 1;
        return isSymmetricUtil(A,A);
    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode(6);
            A.left = new TreeNode(3);
        A.left.left = new TreeNode(2);
        A.left.right = new TreeNode(5);
        A.right = new TreeNode(3);
        A.right.right = new TreeNode(2);
        A.right.left = new TreeNode(5);

        out(isSymmetric(A));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        6
//    3      7
//  2   5   5    9


