package com.neetesh.InterviewBit.Trees;

import java.util.*;

public class BalancedBinaryTree {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{

        int height ;

        public Node(int distance) {
            this.height = distance;
        }
    }

    public static int isBalanced(TreeNode A) {
        if(checkBalancedTree(A,new Node(0))){
            return 1;
        }
        return 0;
    }

    public static boolean checkBalancedTree(TreeNode a,Node h) {
        if(a==null){
            h.height = 0;
            return true;
        }

        Node lh = new Node(0);
        Node rh = new Node(0);
        boolean left = checkBalancedTree(a.left,lh);
        boolean right = checkBalancedTree(a.right,rh);

        int k = new Integer(Math.max(rh.height,lh.height)) + new Integer(1);
        h.height = (k);
        if(left && right && Math.abs(lh.height-rh.height)<=1)return true;
        return false;
    }

    public static void main(String[] args) {
        TreeNode a = new TreeNode(6);
            a.left = new TreeNode(3);
        a.left.left = new TreeNode(2);
        a.left.right = new TreeNode(5);
        a.right = new TreeNode(7);
//        a.right.right = new TreeNode(9);
        out(isBalanced(a));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//        6
//    3      7
//  2   5       9
