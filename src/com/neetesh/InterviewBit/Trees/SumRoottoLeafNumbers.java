package com.neetesh.InterviewBit.Trees;

import java.util.ArrayList;

public class SumRoottoLeafNumbers {
    static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        public TreeNode(int val) {
            this.val = val;
            this.left = null;
            this.right = null;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("TreeNode{");
            sb.append("x=").append(val);
            sb.append('}');
            return sb.toString();
        }

    }

    static class Node{
        int sum;

        public Node(int d1) {
           this.sum = d1;
        }
    }

    public static int sumNumbers(TreeNode A) {
        ArrayList<Integer> res = new ArrayList<>();
        res.add(0);
        int path=0;
        sumNumbersUtil(A,path,res);
        return res.get(0);
    }

    public static int sumNumbersUtil(TreeNode A, int path,ArrayList<Integer> res) {

        if (A==null) {
            return 0;
        }
        path = (path*10+A.val)%1003;
        if(A.left==null && A.right==null){
            res.set(0,(res.get(0)% 1003+path% 1003)% 1003);
            path/=10;
            return 1;
        }

        if(A.left!=null){
            sumNumbersUtil(A.left,path,res);
        }
        if(A.right!=null){
            sumNumbersUtil(A.right,path,res);
        }
        path/=10;


        return 0;
    }

    public static void main(String[] args) {
        TreeNode A = new TreeNode(5);
        A.left = new TreeNode(4);
        A.right = new TreeNode(8);
        A.left.left = new TreeNode(1);
        A.left.left.right = new TreeNode(2);
        A.left.left.left = new TreeNode(7);
        A.right.right = new TreeNode(4);
        A.right.right.right = new TreeNode(1);
        A.right.right.left = new TreeNode(5);
        A.right.left = new TreeNode(3);

//        TreeNode A = new TreeNode(1000);
//        A.left = new TreeNode(2000);
//        A.left.left = new TreeNode(-3001);

        out(sumNumbers(A));
    }
    static void out(Object l) {
        System.out.println(l);
    }

}
//            5
//          /  \
//         4    8
//        /    / \
//        1   3  4
//       / \    / \
//     7    2  5   1


