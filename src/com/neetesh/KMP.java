package com.neetesh;

import java.util.ArrayList;
import java.util.Collections;

public class KMP {
    ArrayList<Integer>lps;
    public void computeLPSArray(String pattern){
        lps = new ArrayList<Integer>(Collections.nCopies(pattern.length(), 0));
        int patternLen = pattern.length();
        int i = 1;
        int len = 0;
        while(i<patternLen){
            if(pattern.charAt(i) == pattern.charAt(len)){
                len++;
                lps.set(i,len);
                i++;
            }else{
                if(len==0){
                    lps.set(i,len);
                    i++;
                }else{
                    len = lps.get(len-1);
                }
            }
        }
        System.out.println("LPS :- "+lps);
    }

    public void KMPSearch(){
        String text = "ABABDABACDABABCABABABABCABABABABCABABABABCABAB";
        String pattern = "ABABCABAB";
        computeLPSArray(pattern);
        computeMatching(text, pattern);

    }

    private void computeMatching(String text , String pattern) {
        int textLen = text.length();
        int i = 0 ;
        int j = 0;
        while (i<textLen){
            if(text.charAt(i) == pattern.charAt(j)){
                i++;j++;
            }

            if(j==pattern.length()){
                System.out.println("Pattern Found "+(i-j));
                j = lps.get(j-1);
            }

            if(i< textLen && text.charAt(i) != pattern.charAt(j)){
                if(j!=0){
                    j = lps.get(j-1);
                }else{
                    i++;
                }
            }
        }
    }

    public static void main(String[] args) {
       KMP kmp = new KMP();
       kmp.KMPSearch();
    }
}
